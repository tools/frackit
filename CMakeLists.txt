# minimum required cmake version
cmake_minimum_required(VERSION 3.3)

# project name
project(frackit VERSION 1.3.0)

# project version
set(FRACKIT_VERSION ${frackit_VERSION} CACHE STRING "project version")
set(FRACKIT_VERSION_MAJOR ${frackit_VERSION_MAJOR} CACHE STRING "major project version")
set(FRACKIT_VERSION_MINOR ${frackit_VERSION_MINOR} CACHE STRING "minor project version")

# make sure our own modules are found
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${PROJECT_SOURCE_DIR}/cmake/modules")

# declare possible options
option(FRACKIT_CONFIG_FOR_DOCKER "Use compiler flags suitable for docker containers" OFF)
option(FRACKIT_ENABLE_PYTHONBINDINGS "Enable python bindings for Frackit" ON)
option(FRACKIT_BUILD_COVERAGE "Build coverage report" OFF)
option(FRACKIT_BUILD_DOC "Build documentation" ON)

# include the macros
include(FrackitMacros)

# compiler definitions
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_FLAGS_DEBUG "-O0 -g -ggdb -Wall -Wextra -Wno-unused-parameter -Wno-sign-compare")
set(CMAKE_CXX_FLAGS_RELWITHDEBINFO "$GXX_RELEASE_OPTS $GXX_RELEASE_WARNING_OPTS -g -ggdb -Wall")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_C_FLAGS} -std=c++17 -O3 -g0 -Wall \
                                              -funroll-loops \
                                              -DNDEBUG=1 -fno-strict-aliasing \
                                              -fstrict-overflow -fno-finite-math-only")

# if building for a docker container, do not use native flag
if (FRACKIT_CONFIG_FOR_DOCKER)
    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -march=x86-64 -mtune=generic")
else ()
    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -march=native")
endif ()

# use release flags per default unless coverage is enabled
if (NOT CMAKE_BUILD_TYPE)
    if (NOT FRACKIT_BUILD_COVERAGE)
        set(CMAKE_BUILD_TYPE Release)
    else ()
        set(CMAKE_BUILD_TYPE Debug)
    endif ()
endif ()

# (maybe) add coverage flags
if (FRACKIT_BUILD_COVERAGE)
    include(CodeCoverage)
    frackit_append_coverage_flags()
endif()

####################
# find openCASCADE #
####################

# TODO: In case we can separate functionality that doesn't require OCC,
#       we could just warn and continue instead of aborting when sources
#       or libraries are not found. However, the code currently has
#       no checks if OCC was found or not so it would fail. Moreover,
#       currently there is little to do only with the stuff that does not
#       require OCC at all.

# if a user-specified location of OCC was provided, look in that location
if (OCC_INSTALL_DIR)
    find_path(OCC_INC "Standard_Version.hxx"
              PATHS ${OCC_INSTALL_DIR}
              PATH_SUFFIXES opencascade include/opencascade occt/
              NO_DEFAULT_PATH)

    if (NOT OCC_INC)
        message(WARNING "Could not find OpenCASCADE sources in the provided installation folder ${OCC_INSTALL_DIR}.")
    endif ()
endif ()

# (maybe) look for system-wide installation
if (NOT OCC_INC)
    find_path(OCC_INC "Standard_Version.hxx" PATH_SUFFIXES opencascade include/opencascade occt/)

    if (NOT OCC_INC)
        message(FATAL_ERROR "Could not find OpenCASCADE sources in system paths.")
    endif ()
endif ()

# occ includes were found, get version
include_directories( ${OCC_INC} )
file(STRINGS ${OCC_INC}/Standard_Version.hxx
     OCC_MAJOR REGEX "#define OCC_VERSION_MAJOR.*")
file(STRINGS ${OCC_INC}/Standard_Version.hxx
     OCC_MINOR REGEX "#define OCC_VERSION_MINOR.*")
file(STRINGS ${OCC_INC}/Standard_Version.hxx
     OCC_MAINT REGEX "#define OCC_VERSION_MAINTENANCE.*")
if (OCC_MAJOR AND OCC_MINOR AND OCC_MAINT)
    string(REGEX MATCH "[0-9]+" OCC_MAJOR "${OCC_MAJOR}")
    string(REGEX MATCH "[0-9]+" OCC_MINOR "${OCC_MINOR}")
    string(REGEX MATCH "[0-9]+" OCC_MAINT "${OCC_MAINT}")
    set(OCC_VERSION "${OCC_MAJOR}.${OCC_MINOR}.${OCC_MAINT}")
    message(STATUS "Found OpenCASCADE version ${OCC_VERSION} in ${OCC_INC}")
else ()
    message(WARNING "Could not extract OpenCASCADE version information.")
endif  ()

# find required OCC toolkits
set(OCC_LIBS_REQUIRED
    # FoundationClasses
    TKernel TKMath
    # ModelingData
    TKG2d TKG3d TKGeomBase TKBRep
    # ModelingAlgorithms
    TKGeomAlgo TKTopAlgo TKPrim TKBO)

# function to search occ toolkit
function(searchOCCToolkit OCC_TK IS_REQUIRED)
    if (OCC_INSTALL_DIR)
        find_library(OCC_LIB ${OCC_TK}
                     PATHS ${OCC_INSTALL_DIR}
                     PATH_SUFFIXES lib ${OCC_SYS_NAME}/lib ${OCC_SYS_NAME}/vc8/lib
                     NO_DEFAULT_PATH)

        if (NOT OCC_LIB)
            message(WARNING "Could not find ${OCC_TK} in provided installation folder ${OCC_INSTALL_DIR}.")
        endif()
    endif ()

    if (NOT OCC_LIB)
        find_library(OCC_LIB ${OCC_TK}
                     PATH_SUFFIXES lib ${OCC_SYS_NAME}/lib ${OCC_SYS_NAME}/vc8/lib)

        if (NOT OCC_LIB AND IS_REQUIRED)
            message(FATAL_ERROR "Could not find required OCC toolkit ${OCC_TK}.")
        elseif (NOT OCC_LIB)
            message(WARNING "Could not find OCC toolkit ${OCC_TK}.")
        endif ()
    endif ()

    if (OCC_LIB)
        message(STATUS "Found ${OCC_TK} at ${OCC_LIB}")
        list(APPEND OCC_LIBS ${OCC_LIB})
        set(OCC_LIBS "${OCC_LIBS}" PARENT_SCOPE)
    endif()

    unset(OCC_LIB CACHE)
endfunction()

message(STATUS "Looking for required OCC toolkits")
foreach (OCC ${OCC_LIBS_REQUIRED})
    searchOCCToolkit(${OCC} TRUE OCC_LIBS)
endforeach ()

if (OCC_ADDITIONAL_TOOLKITS)
    message(STATUS "Looking for additional user-specified OCC toolkits")
    foreach (OCC ${OCC_ADDITIONAL_TOOLKITS})
        searchOCCToolkit(${OCC} FALSE OCC_LIBS)
    endforeach ()
endif ()

###################
# Python bindings #
###################

# python and pybind11 for python bindings
find_package(PythonInterp 3)
find_package(pybind11)

if (pybind11_FOUND)
    message(STATUS "Found pybind11 in ${pybind11_INCLUDE_DIR}")
endif()

if (FRACKIT_ENABLE_PYTHONBINDINGS AND NOT PYTHONINTERP_FOUND)
    set(FRACKIT_ENABLE_PYTHONBINDINGS OFF)
    message("Python interpreter not found. This is required to build the python library.")
elseif (FRACKIT_ENABLE_PYTHONBINDINGS AND NOT pybind11_FOUND)
    set(FRACKIT_ENABLE_PYTHONBINDINGS OFF)
    message("Pybind11 is required to build the python library.")
endif ()

# TODO: make sure pip is installed

#################
# Documentation #
#################

# check if Doxygen is installed
find_package(Doxygen)

if (FRACKIT_BUILD_DOC AND DOXYGEN_FOUND)
    # set input and output files
    set(DOXYGEN_IN ${CMAKE_CURRENT_SOURCE_DIR}/doc/doxygen/Doxyfile.in)
    set(DOXYGEN_OUT ${CMAKE_CURRENT_BINARY_DIR}/doc/doxygen/Doxyfile)

    # request to configure the file
    configure_file(${DOXYGEN_IN} ${DOXYGEN_OUT} @ONLY)
    message(STATUS "Doxygen build started")

    # target for docu
    add_custom_target( doc_doxygen
                       COMMAND ${DOXYGEN_EXECUTABLE} ${DOXYGEN_OUT}
                       WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
                       COMMENT "Generating API documentation with Doxygen"
                       VERBATIM )
elseif (NOT DOXYGEN_FOUND AND FRACKIT_BUILD_DOC)
    message(WARNING "Doxygen needs to be installed to generate the doxygen documentation")
endif ()

##################
# Directory tree #
##################

# include the sources
include_directories( ${CMAKE_SOURCE_DIR} )

# include subdirectories
add_subdirectory(appl)
add_subdirectory(cmake)
add_subdirectory(doc)
add_subdirectory(frackit)
if (FRACKIT_ENABLE_PYTHONBINDINGS)
    add_subdirectory(python)
endif ()
add_subdirectory(test)

###################################
# target for code coverage report #
###################################

if (FRACKIT_BUILD_COVERAGE)
    frackit_setup_target_for_coverage_xml(NAME coverage CMD_ARGS -j 4)
endif ()
