// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup EntityNetwork
 * \brief Class representing a network of entities contained in
 *        one or multiple embedding subdomains.
 */
#ifndef FRACKIT_CONTAINED_ENTITY_NETWORK_HH
#define FRACKIT_CONTAINED_ENTITY_NETWORK_HH

#include "entitynetwork.hh"

namespace Frackit {

/*!
 * \ingroup EntityNetwork
 * \brief Class representing a network of entities contained in
 *        one or multiple embedding subdomains.
 * \deprecated Use EntityNetwork instead
 */
using ContainedEntityNetwork [[deprecated("Use EntityNetwork instead")]]
= EntityNetwork<>;

} // end namespace Frackit

#endif // FRACKIT_CONTAINED_ENTITY_NETWORK_HH
