// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup EntityNetwork
 * \brief Types that can be used to represent fragments of the different
 *        constituents of an entity network, that is, the network entities,
 *        their intersections, junctions of intersections as well as embedding
 *        domains.
 */
#ifndef FRACKIT_ENTITY_NETWORK_CONSTITUENTS_HH
#define FRACKIT_ENTITY_NETWORK_CONSTITUENTS_HH

#include <TopoDS_Shape.hxx>

namespace Frackit::EntityNetworkConstituents {

/*!
 * \ingroup EntityNetwork
 * \brief Base class to represent constituents of entity networks.
 *        Consists of a shape that describes a fragment of some primary
 *        entity, and a unique index within the set of fragments of the
 *        same sort.
 */
class Constituent
{
public:
    using Shape = TopoDS_Shape;

    Constituent(const Shape& s, std::size_t idx) : shape_(s) , index_(idx) {}
    Constituent(Shape&& s, std::size_t idx) : shape_(std::move(s)) , index_(idx) {}

    const Shape& shape() const { return shape_; }
    std::size_t index() const { return index_; }

private:
    Shape shape_;
    std::size_t index_;
};

class SubDomainFragment : public Constituent { public: using Constituent::Constituent; };
class EntityFragment : public Constituent { public: using Constituent::Constituent; };
class IntersectionFragment : public Constituent { public: using Constituent::Constituent; };
class IntersectionJunction : public Constituent { public: using Constituent::Constituent; };

} // end namespace Frackit::EntityNetworkConstituents

#endif // FRACKIT_ENTITY_NETWORK_CONSTITUENTS_HH
