// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup EntityNetwork
 * \brief Class representing a network of entities, consisting
 *        of fragments of entities, their intersections, and in
 *        3d also of intersections of intersections. If the network
 *        is contained in a domain, the domain fragments around the
 *        network are also accessible.
 */
#ifndef FRACKIT_ENTITY_NETWORK_HH
#define FRACKIT_ENTITY_NETWORK_HH

#include <vector>
#include <algorithm>
#include <stdexcept>
#include <unordered_map>

#include <TopTools_DataMapOfShapeInteger.hxx>
#include <TopTools_ListOfShape.hxx>
#include <TopoDS_Shape.hxx>
#include <TopoDS.hxx>

#include <frackit/common/id.hh>
#include <frackit/common/iteratorrange.hh>
#include <frackit/common/iteratorfacades.hh>
#include <frackit/occ/breputilities.hh>

#include "constituents.hh"

namespace Frackit {

/*!
 * \ingroup EntityNetwork
 * \brief Default traits class for entity networks
 */
struct EntityNetworkDefaultTraits
{
    // constituent types
    using SubDomainFragment = EntityNetworkConstituents::SubDomainFragment;
    using EntityFragment = EntityNetworkConstituents::EntityFragment;
    using IntersectionFragment = EntityNetworkConstituents::IntersectionFragment;
    using IntersectionJunction = EntityNetworkConstituents::IntersectionJunction;
};

/*!
 * \nosubgrouping
 * \ingroup EntityNetwork
 * \relates EntityNetworkBuilder
 * \brief Class representing a network of entities, consisting
 *        of fragments of entities, their intersections, and in
 *        3d also of intersections of intersections. If the network
 *        is contained in a domain, the domain fragments around the
 *        network are also accessible.
 *
 * \note It is distinguished between fragment indices and persistent
 *       entity identifiers. The persistent identifiers correspond to
 *       the following, and may be composed of several fragments:<br>
 *       Subdomains    -> user-defined identifiers<br>
 *       entities      -> ids for the user-inserted primary entities<br>
 *       intersections -> ids of the intersections of entities prior
 *                        to fragmentation with the subdomains<br>
 *       junctions     -> ids of intersection junctions prior to fragmentation
 *                        with the subdomains<br>
 *       In case one defines a non-confining subdomain, fragmentation with the
 *       subdomains can lead to further subdivision of entities or intersections,
 *       and each resulting new fragment then maps to the uniqe identifiers above.
 * \note Use the EntityNetworkBuilder class for construction.
 * \tparam T traits class definind the network constituent types
 */
template<class T = EntityNetworkDefaultTraits>
class EntityNetwork
{
    using ThisType = EntityNetwork;
    static constexpr unsigned int unspecified = 0;

    // container types used
    template<class V> using Container = std::vector<V>;
    template<class V> using Iterator = typename Container<V>::const_iterator;

    // index types used
    using Idx = std::size_t;
    using IndexList = Container<Idx>;
    using AdjacencyList = std::vector<IndexList>;
    using SubFragmentsMap = std::unordered_map<Idx, IndexList>;

    // types required for construction
    using NeighborsMap = std::unordered_map<Idx, IndexList>;
    using ShapeMap = TopTools_DataMapOfShapeInteger;
    using ShapeMapIt = TopTools_DataMapIteratorOfDataMapOfShapeInteger;

    // iterator over fragments by means of an index list
    template<class C>
    using IndexedConstituentIterator = RandomAccessContainerIndexedIterator<Container<C>, IndexList>;

    // constituent types
    using SubDomainFragment = typename T::SubDomainFragment;
    using EntityFragment = typename T::EntityFragment;
    using IntersectionFragment = typename T::IntersectionFragment;
    using IntersectionJunction = typename T::IntersectionJunction;

public:
    //! export the underlying traits
    using Traits = T;

    /*!
     * \brief Constructor for non-contained entity networks.
     *        Construction occurs from OpenCASCADE shape maps.
     *        These contain shape fragments as keys, which map to the unique
     *        identifiers of the primary entities that these fragments belong to.
     * \note Use the EntityNetworkBuilder class for convenient construction.
     *
     * \param entityDim Dimension of the network entities
     * \param entityFragments Map containing the entity fragments
     * \param isectionFragments Map containing the entity intersection
     * \param junctions Map containing the intersection junctions
     * \param isNeighbors Adjacency information on which primary entities are
     *                    neighbors to which primary entity intersections
     * \param juncNeighbors Adjacency information on which primary entity intersections
     *                      are neighbors to which primary junction identifiers
     */
    EntityNetwork(int entityDim,
                  const ShapeMap& entityFragments,
                  const ShapeMap& isectionFragments,
                  const ShapeMap& junctions,
                  const NeighborsMap& isNeighbors,
                  const NeighborsMap& juncNeighbors)
    : entityDimension_(entityDim)
    , domainDimension_(unspecified)
    {
        initialize_(entityFragments_, entitySubFragmentsMap_, entityFragmentIdMap_, entityFragments);
        initialize_(isectionFragments_, isectionSubFragmentsMap_, isectionFragmentIdMap_, isectionFragments);
        initialize_(junctions_, junctionFragmentIdMap_, junctions);
        initIntersectionNeighborsMap_(isNeighbors);
        initJunctionsNeighborsMap_(juncNeighbors);
        checkValidity_();
    }

    /*!
     * \brief Constructor for contained entity networks.
     *        Construction occurs from OpenCASCADE shape maps.
     *        These contain shape fragments as keys, which map to the unique
     *        identifiers of the primary entities that these fragments belong to.
     * \note Use the EntityNetworkBuilder class for construction.
     *
     * \param entityDim Dimension of the network entities
     * \param domainDim Dimension of the embedding domain
     * \param subDomainFragments Map containing the subdomain fragments
     * \param entityFragments Map containing the entity fragments
     * \param isectionFragments Map containing the entity intersection
     * \param junctions Map containing the intersection junctions
     * \param isNeighbors Adjacency information on which primary entities are
     *                    neighbors to which primary entity intersections
     * \param juncNeighbors Adjacency information on which primary entity intersections
     *                      are neighbors to which primary junction identifiers
     */
    EntityNetwork(int entityDim, int domainDim,
                  const ShapeMap& subDomainFragments,
                  const ShapeMap& entityFragments,
                  const ShapeMap& isectionFragments,
                  const ShapeMap& junctions,
                  const NeighborsMap& isNeighbors,
                  const NeighborsMap& juncNeighbors)
    : entityDimension_(entityDim)
    , domainDimension_(domainDim)
    {
        initialize_(domainFragments_, domainSubFragmentsMap_, domainFragmentIdMap_, subDomainFragments);
        initialize_(entityFragments_, entitySubFragmentsMap_, entityFragmentIdMap_, entityFragments);
        initialize_(isectionFragments_, isectionSubFragmentsMap_, isectionFragmentIdMap_, isectionFragments);
        initialize_(junctions_, junctionFragmentIdMap_, junctions);
        initIntersectionNeighborsMap_(isNeighbors);
        initJunctionsNeighborsMap_(juncNeighbors);
        checkValidity_();
    }

    /** \name Basic properties of the entity network
     */
    ///@{

    //! Returns the dimension of the network entities
    int entityDimension() const
    { return entityDimension_; }

    //! Returns the dimension of the embedding domain (if defined)
    int domainDimension() const
    {
        if (!domainDimSet_())
            throw std::runtime_error("No embedding domain was defined");
        return domainDimension_;
    }

    //! Returns true if the network is embedded in a domain
    bool isContained() const
    { return domainDimSet_() && domainFragments_.size() > 0; }

    ///@}

    /** \name Return functions for the size of the network.
     *        The functions with the suffix <EM>Fragments</EM> return the
     *        overall number of fragments of a particular constituent, while
     *        the ones without suffix return the number of primary constituents,
     *        that is, the number of user-defined subdomains or entities, or the
     *        number of intersections of primary entities.
     */
    ///@{

    std::size_t numSubDomains() const { return domainSubFragmentsMap_.size(); }
    std::size_t numSubDomainFragments() const { return domainFragments_.size(); }

    std::size_t numEntities() const { return entitySubFragmentsMap_.size(); }
    std::size_t numEntityFragments() const { return entityFragments_.size(); }

    std::size_t numIntersections() const { return isectionSubFragmentsMap_.size(); }
    std::size_t numIntersectionFragments() const { return isectionFragments_.size(); }
    std::size_t numIntersectionJunctions() const { return junctions_.size(); }

    ///@}

    /** \name Iterator ranges over the fragments of the network
     */
    ///@{

    //! Range generator for iteration over all domain fragments
    friend IteratorRange<Iterator<SubDomainFragment>> subDomainFragments(const ThisType& network)
    { return {network.domainFragments_.begin(), network.domainFragments_.end()}; }

    //! Range generator for iteration over all entity fragments
    friend IteratorRange<Iterator<EntityFragment>> entityFragments(const ThisType& network)
    { return {network.entityFragments_.begin(), network.entityFragments_.end()}; }

    //! Range generator for iteration over all intersection fragments
    friend IteratorRange<Iterator<IntersectionFragment>> intersectionFragments(const ThisType& network)
    { return {network.isectionFragments_.begin(), network.isectionFragments_.end()}; }

    //! Range generator for iteration over all intersection junctions
    friend IteratorRange<Iterator<IntersectionJunction>> intersectionJunctions(const ThisType& network)
    { return {network.junctions_.begin(), network.junctions_.end()}; }

    ///@}

    /** \name Map fragments of the network back to their primary identifiers
     */
    ///@{

    Id subDomainId(const SubDomainFragment& fragment) const
    { return Id(domainFragmentIdMap_[fragment.index()]); }

    Id entityId(const EntityFragment& fragment) const
    { return Id(entityFragmentIdMap_[fragment.index()]); }

    Id intersectionId(const IntersectionFragment& fragment) const
    { return Id(isectionFragmentIdMap_[fragment.index()]); }

    Id intersectionJunctionId(const IntersectionJunction& fragment) const
    { return Id(junctionFragmentIdMap_[fragment.index()]); }

    ///@}

    /** \name Iterator ranges over fragments for a specific identifier
     */
    ///@{

    friend IteratorRange<IndexedConstituentIterator<SubDomainFragment>>
    subDomainFragments(const ThisType& network, Id subDomainId)
    {
        using It = IndexedConstituentIterator<SubDomainFragment>;
        return {It(network.domainSubFragmentsMap_.at(subDomainId.get()).begin(), network.domainFragments_),
                It(network.domainSubFragmentsMap_.at(subDomainId.get()).end(), network.domainFragments_)};
    }

    friend IteratorRange<IndexedConstituentIterator<EntityFragment>>
    entityFragments(const ThisType& network, Id entityId)
    {
        using It = IndexedConstituentIterator<EntityFragment>;
        return {It(network.entitySubFragmentsMap_.at(entityId.get()).begin(), network.entityFragments_),
                It(network.entitySubFragmentsMap_.at(entityId.get()).end(), network.entityFragments_)};
    }

    friend IteratorRange<IndexedConstituentIterator<IntersectionFragment>>
    intersectionFragments(const ThisType& network, Id intersectionId)
    {
        using It = IndexedConstituentIterator<IntersectionFragment>;
        return {It(network.isectionSubFragmentsMap_.at(intersectionId.get()).begin(), network.isectionFragments_),
                It(network.isectionSubFragmentsMap_.at(intersectionId.get()).end(), network.isectionFragments_)};
    }

    ///@}

    /** \name Iterator ranges over adjacent or embedded fragments of other constituents
     */
    ///@{

    //! Iterate over the intersection fragments in which the given entity fragment intersects with others
    friend IteratorRange<IndexedConstituentIterator<IntersectionFragment>>
    embeddedFragments(const ThisType& network, const EntityFragment& entityFragment)
    {
        using It = IndexedConstituentIterator<IntersectionFragment>;
        return {It(network.entityFragmentIntersections_[entityFragment.index()].begin(), network.isectionFragments_),
                It(network.entityFragmentIntersections_[entityFragment.index()].end(), network.isectionFragments_)};
    }

    //! Iterate over the junctions that the given intersection fragment is connected to
    friend IteratorRange<IndexedConstituentIterator<IntersectionJunction>>
    embeddedFragments(const ThisType& network, const IntersectionFragment& isFragment)
    {
        using It = IndexedConstituentIterator<IntersectionJunction>;
        return {It(network.intersectionFragmentJunctions_[isFragment.index()].begin(), network.junctions_),
                It(network.intersectionFragmentJunctions_[isFragment.index()].end(), network.junctions_)};
    }

    //! Iterate over the entity fragments that intersect in the given intersection fragment
    friend IteratorRange<IndexedConstituentIterator<EntityFragment>>
    adjacentFragments(const ThisType& network, const IntersectionFragment& isFragment)
    {
        using It = IndexedConstituentIterator<EntityFragment>;
        return {It(network.isNeighbors_[isFragment.index()].begin(), network.entityFragments_),
                It(network.isNeighbors_[isFragment.index()].end(), network.entityFragments_)};
    }

    //! Iterate over the intersection fragments that intersect in the given junction
    friend IteratorRange<IndexedConstituentIterator<IntersectionFragment>>
    adjacentFragments(const ThisType& network, const IntersectionJunction& junction)
    {
        using It = IndexedConstituentIterator<IntersectionFragment>;
        return {It(network.juncNeighbors_[junction.index()].begin(), network.isectionFragments_),
                It(network.juncNeighbors_[junction.index()].end(), network.isectionFragments_)};
    }

    ///@}

private:
    bool domainDimSet_() const
    { return domainDimension_ != unspecified; }

    void checkValidity_() const
    {
        if (domainDimSet_() && domainDimension_ < 1)
            throw std::runtime_error("Invalid domain dimension");

        if (entityDimension_ < 1 || entityDimension_ > 2)
            throw std::runtime_error("Invalid entity dimension");

        if (entityDimension_ < 2)
            if (!juncNeighbors_.empty() || !junctions_.empty())
                throw std::runtime_error("Junctions must not occur for entity dimension < 2");
    }

    // forward declaration of the data members initializer functions
    template<class C>
    void initialize_(Container<C>& constituentContainer,
                     SubFragmentsMap& subFragmentsMap,
                     IndexList& fragmentToIdMap,
                     const ShapeMap& inputMap);
    template<class C>
    void initialize_(Container<C>& constituentContainer,
                     IndexList& fragmentToIdMap,
                     const ShapeMap& inputMap);
    void initIntersectionNeighborsMap_(const NeighborsMap& isNeighborIds);
    void initJunctionsNeighborsMap_(const NeighborsMap& juncNeighborIds);

    unsigned int entityDimension_;
    unsigned int domainDimension_;

    Container<SubDomainFragment> domainFragments_;
    Container<EntityFragment> entityFragments_;
    Container<IntersectionFragment> isectionFragments_;
    Container<IntersectionJunction> junctions_;

    SubFragmentsMap domainSubFragmentsMap_;
    SubFragmentsMap entitySubFragmentsMap_;
    SubFragmentsMap isectionSubFragmentsMap_;

    IndexList domainFragmentIdMap_;
    IndexList entityFragmentIdMap_;
    IndexList isectionFragmentIdMap_;
    IndexList junctionFragmentIdMap_;

    AdjacencyList entityFragmentIntersections_;
    AdjacencyList intersectionFragmentJunctions_;

    AdjacencyList isNeighbors_;
    AdjacencyList juncNeighbors_;
};

// Implementation of the initializer functions for the constituent vectors
template<class T> template<class C>
void EntityNetwork<T>::initialize_(Container<C>& constituentContainer,
                                   SubFragmentsMap& subFragmentsMap,
                                   IndexList& fragmentToIdMap,
                                   const ShapeMap& inputMap)
{
    Idx idx = 0;
    constituentContainer.reserve(inputMap.Size());
    fragmentToIdMap.reserve(inputMap.Size());

    for (ShapeMapIt it(inputMap); it.More(); it.Next())
    {
        constituentContainer.push_back(C{it.Key(), idx});
        subFragmentsMap[it.Value()].push_back(idx);
        fragmentToIdMap.push_back(it.Value());
        idx++;
    }
}

template<class T> template<class C>
void EntityNetwork<T>::initialize_(Container<C>& constituentContainer,
                                   IndexList& fragmentToIdMap,
                                   const ShapeMap& inputMap)
{
    Idx idx = 0;
    constituentContainer.reserve(inputMap.Size());
    fragmentToIdMap.reserve(inputMap.Size());
    for (ShapeMapIt it(inputMap); it.More(); it.Next())
    {
        constituentContainer.push_back(C{it.Key(), idx});
        fragmentToIdMap.push_back(it.Value());
        idx++;
    }
}

// Implementation of the initializer function of the neighbor maps
template<class T>
void EntityNetwork<T>::initIntersectionNeighborsMap_(const NeighborsMap& isNeighborIds)
{
    isNeighbors_.resize(isectionFragments_.size());
    entityFragmentIntersections_.resize(entityFragments_.size());
    for (const auto& isFragment : intersectionFragments(*this))
    {
        const auto isId = intersectionId(isFragment);
        for (const auto& neighborId : isNeighborIds.at(isId.get()))
            for (const auto& entFragment : entityFragments(*this, Id(neighborId)))
                for (const auto& e : OCCUtilities::getEdges(entFragment.shape()))
                    if (TopoDS_Shape(e).IsSame(isFragment.shape()))
                    {
                        isNeighbors_[isFragment.index()].push_back(entFragment.index());
                        entityFragmentIntersections_[entFragment.index()].push_back(isFragment.index());
                        break;
                    }
    }

    auto makeUnique = [] (auto& v)
    {
        std::sort(v.begin(), v.end());
        v.erase(std::unique(v.begin(), v.end()), v.end());
    };

    auto avoidEmptyIs = [] (auto& v)
    { if (v.empty()) throw std::runtime_error("Empty intersection neighbor map"); };

    for (auto& entry : isNeighbors_) { avoidEmptyIs(entry); makeUnique(entry); }
    for (auto& entry : entityFragmentIntersections_) makeUnique(entry);
}

template<class T>
void EntityNetwork<T>::initJunctionsNeighborsMap_(const NeighborsMap& juncNeighborIds)
{
    juncNeighbors_.resize(junctions_.size());
    intersectionFragmentJunctions_.resize(isectionFragments_.size());
    for (const auto& junction : intersectionJunctions(*this))
    {
        const auto jId = intersectionJunctionId(junction);
        for (const auto& neighborId : juncNeighborIds.at(jId.get()))
            for (const auto& isFragment : intersectionFragments(*this, Id(neighborId)))
                for (const auto& v : OCCUtilities::getVertices(isFragment.shape()))
                    if (TopoDS_Shape(v).IsSame(junction.shape()))
                    {
                        juncNeighbors_[junction.index()].push_back(isFragment.index());
                        intersectionFragmentJunctions_[isFragment.index()].push_back(junction.index());
                        break;
                    }
    }

    auto makeUnique = [] (auto& v)
    {
        std::sort(v.begin(), v.end());
        v.erase(std::unique(v.begin(), v.end()), v.end());
    };

    auto avoidEmptyJ = [] (auto& v)
    { if (v.empty()) throw std::runtime_error("Empty junction neighbor map"); };

    for (auto& entry : juncNeighbors_) { avoidEmptyJ(entry); makeUnique(entry); }
    for (auto& entry : intersectionFragmentJunctions_) makeUnique(entry);
}

} // end namespace Frackit

#endif // FRACKIT_ENTITY_NETWORK_HH
