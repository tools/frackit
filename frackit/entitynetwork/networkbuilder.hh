// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup EntityNetwork
 * \brief Contains builder classes for entity networks.
 */
#ifndef FRACKIT_ENTITY_NETWORK_BUILDER_HH
#define FRACKIT_ENTITY_NETWORK_BUILDER_HH

#include <cmath>
#include <tuple>
#include <vector>
#include <limits>
#include <stdexcept>
#include <unordered_map>
#include <memory>

#include <TopTools_DataMapOfShapeInteger.hxx>
#include <TopTools_ListOfShape.hxx>
#include <TopoDS_Shape.hxx>

#include <frackit/common/id.hh>
#include <frackit/common/idpair.hh>
#include <frackit/geometry/dimension.hh>

#include <frackit/precision/defaultepsilon.hh>
#include <frackit/precision/precision.hh>
#include <frackit/occ/breputilities.hh>

#include "entitynetwork.hh"
#include "containedentitynetwork.hh"

namespace Frackit {

// Forward declaration of the builder class for entity networks
template<class ctype = double, class EN = Frackit::EntityNetwork<>>
class EntityNetworkBuilder;

// Forward declaration of the builder class for contained entity networks
template<class ctype = double, class EN = Frackit::EntityNetwork<>>
class ContainedEntityNetworkBuilder;

/*!
 * \ingroup EntityNetwork
 * \brief Base class for builders of entity networks.
 *        Stores data related to the entities and sub-domains.
 */
template<class ctype, class EN>
class EntityNetworkBuilderBase
{
protected:
    using ShapeList = TopTools_ListOfShape;
    using ShapeIdMap = TopTools_DataMapOfShapeInteger;
    using ShapeIdMapIt = TopTools_DataMapIteratorOfDataMapOfShapeInteger;
    using NeighborsMap = std::unordered_map<std::size_t, std::vector<std::size_t>>;

public:

    /*!
     * \brief If entities are defined for non-confining subdomains,
     *        they are still confined to the overall domain per default.
     *        This function allows changing this behaviour, such that
     *        the entities possibly stand out of the overall domain.
     * \param value true or false
     */
    void setConfineToSubDomainUnion(bool value)
    {
        confineToSubDomainUnion_ = value;
    }

    /*!
     * \brief Define a tolerance value to be used
     *        for boolean operations on the entity shapes.
     * \param eps Tolerance value to be used
     */
    void setEpsilon(ctype eps)
    {
        eps_ = eps;
        epsilonIsSet_ = true;
    }

    /*!
     * \brief Computes and sets a tolerance value using the extents
     *        of ths entities inserted into the network so far.
     */
    void setDefaultEpsilon()
    {
        // if no entities have been set, set default precision
        if (subDomains_.empty() && networks_.empty())
            eps_ = Precision<ctype>::confusion();
        else
        {
            // Use the minimum of all default epsilons of the sub-domains or entities
            eps_ = std::numeric_limits<ctype>::max();
            using std::min;

            if (!subDomains_.empty())
                for (const auto& sdPair : subDomains_)
                    eps_ = min(eps_, defaultEpsilon(sdPair.second));
            else
            {
                for (const auto& idNetworkPair : networks_)
                    for (ShapeIdMapIt it(idNetworkPair.second); it.More(); it.Next())
                        eps_ = min(eps_, defaultEpsilon(it.Key()));
            }
        }

        epsilonIsSet_ = true;
    }

    /*!
     * \brief Returns true if the network has been
     *        built, i.e. if a call to build() has ocurred.
     */
    bool isBuilt() const
    { return built_; }

    /*!
     * \brief Defines a sub-domain which potentially
     *        contains an embedded entity network.
     *        Networks defined for this sub-domain
     *        will not be confined by the sub-domain's
     *        boundary.
     * \param domain The sub-domain geometry
     * \param subDomainId The identifier (index) to be used for this sub-domain
     */
    template<class Domain>
    void addSubDomain(const Domain& domain, Id subDomainId)
    {
        const auto domainDim = getDimension(domain);
        if (!networks_.empty() && domainDim <= entityDimension_)
            throw std::runtime_error("Sub-domain dimension must be greater than entity dimension");
        if (!subDomains_.empty() && domainDim != domainDimension_)
            throw std::runtime_error("Sub-domain dimension does not match to previously added");
        if (subDomains_.find(subDomainId.get()) != subDomains_.end())
            throw std::runtime_error("Sub-domain id already taken!");

        domainDimension_ = domainDim;
        subDomains_[subDomainId.get()] = OCCUtilities::getShape(domain);
        subDomainBoundsNetwork_[subDomainId.get()] = false;
    }

    /*!
     * \brief Defines a sub-domain which potentially
     *        contains an embedded entity network.
     *        Using this interface, the sub-domain is
     *        defined as confining, that is, the
     *        network embedded in it will be confined
     *        by the sub-domain's boundary.
     * \param domain The sub-domain geometry
     * \param subDomainId The identifier (index) to be used for this sub-domain
     */
    template<class Domain>
    void addConfiningSubDomain(const Domain& domain, Id subDomainId)
    {
        addSubDomain(domain, subDomainId);
        subDomainBoundsNetwork_[subDomainId.get()] = true;
    }

    /*!
     * \brief Adds an entity to the network embedded in
     *        the sub-domain with id subDomainId.
     * \param entity The entity geometry
     * \param subDomainId The identifier (index) to be used for this sub-domain
     * \return The id given to the inserted entity.
     */
    template<class Entity>
    Id addSubDomainEntity(const Entity& entity, Id subDomainId)
    { return bindEntity_(subDomainId, entity); }

    /*!
     * \brief Adds a set of entities to the network
     *        embedded in sub-domain with the id subDomainId.
     * \param entities The set of entities
     * \param subDomainId The identifier (index) to be used for this sub-domain
     * \return The range of ids given to the inserted entities
     *         in form of an id pair, containing the first
     *         and last used entity ids.
     * \note If the provided range is empty, the id of the previously inserted
     *       entity is returned. If no entity had been inserted before, the id
     *       is 0, and thus, no valid id (we start counting at 1). In general,
     *       passing empty ranges should be avoided.
     */
    template<class EntityNetwork>
    IdPair addSubDomainEntities(const EntityNetwork& entities, Id subDomainId)
    {
        const auto firstIdx = curEntityIdx_;
        if (std::empty(entities))
            return IdPair{firstIdx-1, firstIdx-1};

        IdPair idPair{firstIdx, firstIdx};
        for (const auto& entity : entities)
            idPair = IdPair(Id{firstIdx}, addSubDomainEntity(entity, subDomainId));

        return idPair;
    }

    /*!
     * \brief Clear registered shapes.
     *        Leaves settings on epsilon and confinement untouched.
     */
    void clear()
    {
        subDomains_.clear();
        subDomainBoundsNetwork_.clear();
        networks_.clear();
        curEntityIdx_ = 1;
        built_ = false;
    }

protected:

    /*!
     * \brief Bind entity to the given network & return its id
     */
    template<class Entity>
    Id bindEntity_(Id sdId, const Entity& e)
    {
        const auto entityDim = getDimension(e);
        if (!networks_.empty() && entityDim != entityDimension_)
            throw std::runtime_error("Entity dimension does not match to previously added");
        if (!subDomains_.empty() && entityDim >= domainDimension_)
            throw std::runtime_error("Entity dimension must be smaller than domain dimension");

        entityDimension_ = entityDim;
        networks_[sdId.get()].Bind(OCCUtilities::getShape(e), curEntityIdx_);
        return Id{curEntityIdx_++};
    }

    /*!
     * \brief Computes the initial fragmentation of the network
     *        resulting from confinement to the (sub-)domains.
     */
    ShapeIdMap confineEntities_()
    {
        ShapeIdMap result;
        for (const auto& indexNetworkPair : networks_)
        {
            const auto subDomainIndex = indexNetworkPair.first;
            const auto& entityIdMap = indexNetworkPair.second;

            // lambda for unconfined entity adding
            auto addUnconfined = [&result, &entityIdMap] ()
            {
                for (ShapeIdMapIt it(entityIdMap); it.More(); it.Next())
                    result.Bind(it.Key(), it.Value());
            };

            // confine network only if sub-domain was set
            if (subDomains_.find(subDomainIndex) == subDomains_.end())
                addUnconfined();
            else
            {
                // confine network with sub-domain or entire domain
                TopTools_ListOfShape domainShapeList;
                if (subDomainBoundsNetwork_.at(subDomainIndex))
                    domainShapeList.Append(subDomains_.at(subDomainIndex));
                else if (confineToSubDomainUnion_)
                    domainShapeList.Append(getSubDomainUnion_());
                else
                { addUnconfined(); continue; }

                const auto entityShapeList = makeShapeList_(entityIdMap);

                BRepAlgoAPI_Common intersection;
                intersection.SetArguments(entityShapeList);
                intersection.SetTools(domainShapeList);
                intersection.SetRunParallel(false);
                intersection.SetFuzzyValue(getEpsilon_());
                intersection.Build();
                if(!intersection.IsDone())
                    throw std::runtime_error("Could not confine network");

                // map fragments to original entity indices
                for (const auto& entityShape : entityShapeList)
                {
                    const auto entityIdx = entityIdMap.Find(entityShape);
                    const bool isDeleted = intersection.IsDeleted(entityShape);
                    const auto& modified = intersection.Modified(entityShape);

                    if (!isDeleted && modified.IsEmpty())
                        result.Bind(entityShape, entityIdx);
                    for (const auto& fragment : modified)
                        if (!result.IsBound(fragment))
                            result.Bind(fragment, entityIdx);
                }
            }
        }

        return result;
    }

    /*!
     * \brief Computes the fragmentation of all entities provided in the given maps
     * \param maps vector of shape maps with entities that are to be fragmented
     * \return vector with the fragments, mapping back to the indices of the given maps
     */
    std::vector<ShapeIdMap> fragment_(const std::vector<const ShapeIdMap*>& maps)
    {
        ShapeList allShapes;
        for (auto mapPtr : maps)
        {
            auto list = makeShapeList_(*mapPtr);
            allShapes.Append(list);
        }

        // fragment all shapes
        BRepAlgoAPI_BuilderAlgo fragmentAlgo;
        fragmentAlgo.SetRunParallel(false);
        fragmentAlgo.SetArguments(allShapes);
        fragmentAlgo.SetFuzzyValue(getEpsilon_());
        fragmentAlgo.Build();
        if(!fragmentAlgo.IsDone())
            throw std::runtime_error("Could not perform fragmentation");

        // map the resulting entities back to the given indices
        std::vector<ShapeIdMap> result(maps.size());
        for (unsigned int i = 0; i < maps.size(); ++i)
        {
            auto& resultMap = result[i];
            for (ShapeIdMapIt it(*(maps[i])); it.More(); it.Next())
            {
                const auto& origShape = it.Key();
                const auto& origIndex = it.Value();

                const bool isDeleted = fragmentAlgo.IsDeleted(origShape);
                const auto& modified = fragmentAlgo.Modified(origShape);

                if (!isDeleted && modified.IsEmpty())
                    resultMap.Bind(origShape, origIndex);

                for (const auto& fragment : modified)
                    if (!resultMap.IsBound(fragment))
                        resultMap.Bind(fragment, origIndex);
            }
        }

        return result;
    }

    /*!
     * \brief Returns a tuple of maps, where:
     *        First map: entity intersection -> unique identifiers
     *        Second map: isection identifier -> primary ids of the adjacent fragments
     */
    std::tuple<ShapeIdMap, NeighborsMap> getIntersections_(const ShapeIdMap& fragments,
                                                           unsigned int fragmentDim)
    {
        ShapeIdMap isShapes;
        NeighborsMap isNeighbors;
        std::size_t curIsId = 1;

        for (ShapeIdMapIt it1(fragments); it1.More(); it1.Next())
        {
            ShapeIdMapIt it2(it1);
            for (it2.Next(); it2.More(); it2.Next())
            {
                for (const auto& s : getCommonShapes_(it1.Key(), it2.Key(), fragmentDim))
                {
                    int isId;
                    if (!isShapes.Find(s, isId))
                    { isId = curIsId++; isShapes.Bind(s, isId); }

                    isNeighbors[isId].push_back(it1.Value());
                    isNeighbors[isId].push_back(it2.Value());
                }
            }
        }

        // make neighbor maps unique
        for (auto& [isId, neighbors] : isNeighbors)
        {
            std::sort(neighbors.begin(), neighbors.end());
            auto endIt = std::unique(neighbors.begin(), neighbors.end());
            neighbors.erase(endIt, neighbors.end());
        }

        return {std::move(isShapes), std::move(isNeighbors)};
    }

    /*!
     * \brief Returns a vector with the shapes of codimension 1
     *        that are common to two given shapes
     */
    std::vector<TopoDS_Shape> getCommonShapes_(const TopoDS_Shape& s1,
                                               const TopoDS_Shape& s2,
                                               unsigned int dim)
    {
        auto cast = [] (const auto& v) { return std::vector<TopoDS_Shape>{v.begin(), v.end()}; };

        using namespace OCCUtilities;
        const auto boundaryDim = dim - 1;
        if (boundaryDim == 0) return cast(getCommonVertices(s1, s2));
        if (boundaryDim == 1) return cast(getCommonEdges(s1, s2));
        if (boundaryDim == 2) return cast(getCommonFaces(s1, s2));
        throw std::runtime_error("Invalid boundary dimension provided");
    }

    /*!
     * \brief Returns the tolerance value.
     */
    ctype getEpsilon_()
    {
        if (!epsilonIsSet_)
            setDefaultEpsilon();
        return eps_;
    }

    /*!
     * \brief Returns the shape of the union of all sub-domains.
     */
    TopoDS_Shape getSubDomainUnion_()
    {
        TopoDS_Shape domainUnion;

        if (subDomains_.size() == 0)
            throw std::runtime_error("No sub-domains defined yet");

        if (subDomains_.size() == 1)
            domainUnion = subDomains_.begin()->second;
        else
        {
            // compute union of all subdomains
            std::vector<TopoDS_Shape> subShapes;
            subShapes.reserve(subDomains_.size());
            for (const auto& sdPair  : subDomains_)
                subShapes.emplace_back(sdPair.second);
            domainUnion = OCCUtilities::fuse(subShapes, getEpsilon_());
        }

        return domainUnion;
    }

    /*!
     * \brief Make a shape list from a shape->id map.
     */
    ShapeList makeShapeList_(const ShapeIdMap& map)
    {
        ShapeList result;
        for (ShapeIdMapIt it(map); it.More(); it.Next())
            result.Append(it.Key());
        return result;
    }

    // User-defined sub-domains and entity networks
    std::unordered_map<std::size_t, TopoDS_Shape> subDomains_;
    std::unordered_map<std::size_t, bool> subDomainBoundsNetwork_;
    std::unordered_map<std::size_t, TopTools_DataMapOfShapeInteger> networks_;

    // keep track of registered entity ids
    std::size_t curEntityIdx_ = 1;

    // dimensionalities
    int domainDimension_;
    int entityDimension_;

    // Epsilon value to be used for fragmentation
    ctype eps_;

    // state variables
    bool epsilonIsSet_ = false;
    bool built_ = false;
    bool confineToSubDomainUnion_ = true;
};

/*!
 * \ingroup EntityNetwork
 * \brief Builder class for entity networks,
 *        which neglect the sub-domains in which
 *        the network is embedded in and only carries
 *        information about the network itself.
 */
template<class ctype, class EN>
class EntityNetworkBuilder
: public EntityNetworkBuilderBase<ctype, EN>
{
public:
    //! Export network type to be built
    using EntityNetwork = EN;

    /*!
     * \brief Adds an entity to the network.
     *        This function can be used when no sub-domain
     *        specifications are made.
     * \note Internally, the entity is added to a dummy
     *       sub-domain with id std::numeric_limits<std::size_t>::max().
     *       Thus, this could lead to clashes if users define sub-domains
     *       with this id. The entities will then be merged into one embedded
     *       network.
     * \return The id of the newly added entity
     */
    template<class Entity>
    Id addEntity(const Entity& entity)
    {
        const auto subDomainId = std::numeric_limits<std::size_t>::max();
        return this->bindEntity_(Id{subDomainId}, entity);
    }

    /*!
     * \brief Adds a set of entities to the network.
     * \return The range of ids given to the inserted entities
     *         in form of an id pair, containing the first
     *         and last used entity ids.
     * \note If the provided range is empty, the id of the previously inserted
     *       entity is returned. If no entity had been inserted before, the id
     *       is 0, and thus, no valid id (we start counting at 1). In general,
     *       passing empty ranges should be avoided.
     */
    template<class EntityNetwork>
    IdPair addEntities(const EntityNetwork& entities)
    {
        const auto firstIdx = this->curEntityIdx_;
        if (std::empty(entities))
            return IdPair{firstIdx-1, firstIdx-1};

        IdPair idPair{firstIdx, firstIdx};
        for (const auto& entity : entities)
            idPair = IdPair(Id{firstIdx}, addEntity(entity));

        return idPair;
    }

    /*!
     * \brief Builds the network.
     */
    const EntityNetwork& build()
    {
        const auto confinedEntities = this->confineEntities_();
        const auto entityFragments = this->fragment_({&confinedEntities})[0];
        auto [isections, isMap] = this->getIntersections_(entityFragments, this->entityDimension_);
        auto [junctions, jMap] = this->getIntersections_(isections, this->entityDimension_-1);

        // create the network
        network_ = std::make_unique<EntityNetwork>(this->entityDimension_,
                                                   entityFragments, isections,
                                                   junctions, isMap, jMap);

        this->built_ = true;
        return *network_;
    }

    /*!
     * \brief Returns the built network.
     */
    const EntityNetwork& network() const
    {
        if (!this->isBuilt())
            throw std::runtime_error("Network has not been built yet!");
        return *network_;
    }

private:
    std::unique_ptr<EntityNetwork> network_;
};

/*!
 * \ingroup EntityNetwork
 * \brief Builder class for entity networks contained
 *        in (possibly multiple) sub-domains.
 */
template<class ctype, class EN>
class ContainedEntityNetworkBuilder
: public EntityNetworkBuilderBase<ctype, EN>
{
    using ParentType = EntityNetworkBuilderBase<ctype, EN>;
    using typename ParentType::ShapeIdMap;

public:

    //! Export network type to be built
    using EntityNetwork = EN;

    /*!
     * \brief Build the network.
     */
    const EntityNetwork& build()
    {
        // maybe print a warning
        if (std::any_of(this->networks_.begin(),
                        this->networks_.end(),
                        [&] (const auto& pair)
                        { return this->subDomains_.find(pair.first) == this->subDomains_.end(); }))
            std::cout << "\t --> build(): there are networks with no sub-domain defined" << std::endl;

        // confine entities and compute intersections before fragmentation with the domain
        const auto confinedEntities = this->confineEntities_();
        const auto entityFragments = this->fragment_({&confinedEntities})[0];

        // find all intersections and find adjacency maps in terms of primary indices
        auto [isections, isMap] = this->getIntersections_(entityFragments, this->entityDimension_);
        auto [junctions, jMap] = this->getIntersections_(isections, this->entityDimension_-1);

        // fragmentation with the subdomains
        ShapeIdMap subDomainMap;
        for (const auto& [id, shape] : this->subDomains_)
            subDomainMap.Bind(shape, id);

        const auto fragments = this->fragment_({&subDomainMap,
                                                &entityFragments,
                                                &isections,
                                                &junctions});

        // create the network
        network_ = std::make_unique<EntityNetwork>(this->entityDimension_,
                                                   this->domainDimension_,
                                                   fragments[0], fragments[1],
                                                   fragments[2], fragments[3],
                                                   isMap, jMap);
        this->built_ = true;
        return *network_;
    }

    /*!
     * \brief Return the built network.
     */
    const EntityNetwork& network() const
    {
        if (!this->isBuilt())
            throw std::runtime_error("Network has not been built yet!");
        return *network_;
    }

private:
    std::unique_ptr<EntityNetwork> network_;
};

} // end namespace Frackit

#endif // FRACKIT_ENTITY_NETWORK_BUILDER_HH
