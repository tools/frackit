// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup IO
 * \brief Backend for Gmsh Syntax, depending on the desired .geo file format version.
 */
#ifndef FRACKIT_GMSH_FORMAT_BACKEND_HH
#define FRACKIT_GMSH_FORMAT_BACKEND_HH

#include <array>
#include <string>
#include <limits>
#include <cassert>
#include <type_traits>

namespace Frackit::Gmsh {

/*!
 * \ingroup IO
 * \brief Backend for writing Gmsh Syntax to files,
 *        depending on the desired .geo file format version.
 */
class GmshFormatBackend
{

public:

    /*!
     * \brief Default constructor.
     */
    GmshFormatBackend()
    {
        setVersion(4, 0);
    }

    /*!
     * \brief Constructor defining the gmsh version
     *        for which a .geo file is to be written.
     * \param majVersion major gmsh version
     * \param minVersion minor gmsh version
     */
    GmshFormatBackend(int maj, int min)
    {
        setVersion(maj, min);
    }

    /*!
     * \brief Update version settings.
     */
    void setVersion(int maj, int min)
    {
        // we use two-digits minor version
        if (min < 10) min *= 10;

        assert(min < 100);
        gmshVersion_ = maj*100 + min;
        assert(gmshVersion_ < 1000);
    }

    /*!
     * \brief Write a physical entity definition.
     * \param geoFile The geoFile to write to
     * \param entityDimension Entity dimension
     * \param physicalId The id to give the physical entity
     * \param elementaryIds Ids of the elementary entities composing the physical entity
     */
    template<class IdList>
    void writePhysicalGeometry(std::ofstream& geoFile,
                               int entityDimension,
                               std::size_t physicalId,
                               const IdList& elementaryIds) const
    {
        static_assert(std::is_integral_v<typename IdList::value_type>,
                      "Ids must be integral types");

        const auto& geomKey = getGeometryKeyword_(entityDimension);
        geoFile << "Physical " << geomKey << "(" << physicalId << ") = ";
        writeIdList_(geoFile, elementaryIds);
        geoFile << ";\n";
    }

    /*!
     * \brief Write variable definition.
     * \param geoFile The geoFile to write to
     * \param varName variable name
     * \param varValue variable value
     * \note We define all variables such that they can be overwritten
     *       via the command line when opening the .geo file with Gmsh
     */
    template<class ValueType>
    void writeVariableDefinition(std::ofstream& geoFile,
                                 const std::string& varName,
                                 const ValueType& varValue) const
    {
        geoFile << "DefineConstant[ " << varName << " = " << varValue << " ];\n";
    }

    /*!
     * \brief Write mesh size definition.
     * \param geoFile The geoFile to write to
     * \param entityDimension Dimension of the entity on which to set the mesh size.
     * \param elementaryIds List of elementary entity ids (must correspond
     *                      to entities with dimension entityDimension)
     * \param size string containing the size or size variable name.
     */
    template<class IdList>
    void writeMeshSizeDefinition(std::ofstream& geoFile,
                                 int entityDimension,
                                 const IdList& idList,
                                 const std::string& size) const
    {
        const auto& meshSizeKey = getMeshSizeKeyword_();
        const auto& geomKey = getGeometryKeyword_(entityDimension);
        geoFile << meshSizeKey << "{ PointsOf{" << geomKey;
        writeIdList_(geoFile, idList);
        geoFile << ";} } = " << size << ";\n";
    }

private:
    template<class IdList>
    void writeIdList_(std::ofstream& geoFile, const IdList& idList) const
    {
        geoFile << "{";
        geoFile << idList[0];
        for (unsigned int j = 1; j < idList.size(); ++j)
            geoFile << ", " << idList[j];
        geoFile << "}";
    }

    std::string getGeometryKeyword_(unsigned int geomDim) const
    {
        assert(geomDim <= 3);
        static const std::array<std::string, 4> keywords = {
            "Point", "Curve", "Surface", "Volume"
        };

        return keywords[geomDim];
    }

    std::string getMeshSizeKeyword_() const
    {
        if (gmshVersion_ < 470)
            return "Characteristic Length";
        return "MeshSize";
    }

    int gmshVersion_;
};

namespace Detail {

class MeshSizeVariableNameHelper
{
    static constexpr double maxDouble = std::numeric_limits<double>::max();
    std::array<std::size_t, 4> counts = {0, 0, 0, 0};
    std::array<double, 4> sizes = {maxDouble, maxDouble, maxDouble, maxDouble};
    std::array<std::string, 4> names = {"subDomainMeshSize_",
                                        "entityMeshSize_",
                                        "intersectionMeshSize_",
                                        "pointMeshSize_"};

public:
    void reset()
    {
        for (auto& c : counts) c = 0;
        for (auto& s : sizes) s = maxDouble;
    }

    bool prepareNew(unsigned int codim, double size)
    {
        if (sizes[codim] > size)
        {
            sizes[codim] = size;
            counts[codim]++;

            const auto pos = names[codim].find_last_of("_");
            const auto suffix = std::to_string(counts[codim]);
            const auto suffixLength = suffix.size();
            names[codim].resize(pos+1 + suffixLength);
            names[codim].replace(pos+1, suffixLength, suffix);

            return true;
        }

        return false;
    }

    const std::string& get(unsigned int codim)
    { return names[codim]; }
};

} // end namespace Detail

} // end namespace Frackit::Gmsh

#endif // FRACKIT_GMSH_BACKEND_HH
