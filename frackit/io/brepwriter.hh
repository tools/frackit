// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup IO
 * \brief Class that writes entity networks into the .brep file format.
 */
#ifndef FRACKIT_BREP_WRITER_HH
#define FRACKIT_BREP_WRITER_HH

#include <string>
#include <stdexcept>
#include <unordered_map>

#include <BRepTools.hxx>
#include <BRep_Builder.hxx>

#include <TopTools_IndexedMapOfShape.hxx>
#include <TopExp_Explorer.hxx>
#include <TopoDS.hxx>

// include all used shapes explicitly
#include <TopoDS_Shape.hxx>
#include <TopoDS_Vertex.hxx>
#include <TopoDS_Edge.hxx>
#include <TopoDS_Wire.hxx>
#include <TopoDS_Face.hxx>
#include <TopoDS_Shell.hxx>
#include <TopoDS_Solid.hxx>
#include <TopoDS_Compound.hxx>

namespace Frackit {

/*!
 * \ingroup IO
 * \brief Class that writes entity networks into the .brep file format.
 *        This creates a single TopoDS_Compound shape in which each sub-shape
 *        is uniquely defined.
 */
class BRepWriter
{
public:

    /*!
     * \brief Construction from an entity network.
     */
    template<class EntityNetwork>
    BRepWriter(const EntityNetwork& network)
    {
        if (network.entityDimension() < 1)
            throw std::runtime_error("Entity dimension < 1 is not supported");
        if (network.isContained())
            if (network.domainDimension() <= network.entityDimension())
                throw std::runtime_error("Domain dimension must be greater than entity dimension");

        makeSubShapeMaps_(network);
        makeCompound_();
    }

    /*!
     * \brief Write the entity network to disk.
     */
    void write(const std::string& fileName) const
    {
        const std::string brepFile = fileName + ".brep";
        BRepTools::Write(compound_, brepFile.c_str());
    }

protected:

    using IndexList = std::vector<std::size_t>;
    using IndexMapType = std::unordered_map<std::size_t, IndexList>;

    /*!
     * \brief Returns the map that maps a primary entity index to the list
     *        of fragment indices that were inserted to the compound for
     *        that primary entity index. The indices refer to the indices
     *        within the compound.
     */
    const IndexMapType& entityToFragmentsMap() const
    { return entityMap_; }

    /*!
     * \brief Returns the map that maps a primary entity intersectionindex to the
     *        list of intersection fragments that were inserted to the compound for
     *        that intersection. The indices refer to the indices within the compound.
     */
    const IndexMapType& entityIntersectionsToFragmentsMap() const
    { return intersectionMap_; }

    /*!
     * \brief Returns the map that maps to each primary sub-domain index the list
     *        of domain fragment indices that were inserted to the compound for
     *        that sub-domain. The indices refer to the indices within the compound.
     */
    const IndexMapType& domainToFragmentsMap() const
    { return domainMap_; }

    /*!
     * \brief Returns the map that maps to each primary junction index the
     *        index of the junction within the compound.
     */
    const IndexMapType& junctionToFragmentsMap() const
    { return junctionMap_; }

private:

    /*!
     * \brief Fill the sub-shape maps with all shapes that describe the network.
     * \note This overload is for contained entity networks.
     */
    template<class EntityNetwork>
    void makeSubShapeMaps_(const EntityNetwork& network)
    {
        if (network.isContained())
        {
            const auto domainDim = network.domainDimension();
            for (const auto& domainFragment : subDomainFragments(network))
                addSubDomain_(domainDim, domainFragment.shape(), network.subDomainId(domainFragment).get());
        }

        const auto entDim = network.entityDimension();
        for (const auto& entityFragment : entityFragments(network))
            addEntity_(entDim, entityFragment.shape(), network.entityId(entityFragment).get());

        const auto isDim = network.entityDimension() - 1;
        for (const auto& isFragment : intersectionFragments(network))
            addIntersection_(isDim, isFragment.shape(), network.intersectionId(isFragment).get());

        for (const auto& junction : intersectionJunctions(network))
            addJunction_(junction.shape(), network.intersectionJunctionId(junction).get());
    }

    /*!
     * \brief Add a domain of a network to the sub-shape maps.
     */
    void addSubDomain_(int domainDim, const TopoDS_Shape& shape, std::size_t id)
    {
        if (domainDim == 3) addSolids_(shape, domainMap_[id]);
        else if (domainDim == 2) addFaces_(shape, domainMap_[id]);
        else throw std::runtime_error("Unsupported domain dimension");
    }

    /*!
     * \brief Add an entity of a network to the sub-shape maps.
     */
    void addEntity_(int entityDim, const TopoDS_Shape& shape, std::size_t id)
    {
        if (entityDim == 2) addFaces_(shape, entityMap_[id]);
        else if (entityDim == 1) addEdges_(shape, entityMap_[id]);
        else throw std::runtime_error("Unsupported entity dimension");
    }

    /*!
     * \brief Add an intersection of a network to the sub-shape maps.
     */
    void addIntersection_(int isDim, const TopoDS_Shape& shape, std::size_t id)
    {
        if (isDim == 1) addEdges_(shape, intersectionMap_[id]);
        else if (isDim == 0) addVertices_(shape, intersectionMap_[id]);
        else throw std::runtime_error("Unsupported intersection dimension");
    }

    /*!
     * \brief Add an intersection of a network to the sub-shape maps.
     */
    void addJunction_(const TopoDS_Shape& shape, std::size_t id)
    { addVertices_(shape, junctionMap_[id]); }

    /*!
     * \brief Add solids to the sub-shape maps
     * \param shape The shape of which the solids are to be extracted
     * \param idxList List to which to append the indices of the solids
     *                of the given shape within the local maps.
     */
    void addSolids_(const TopoDS_Shape& shape, std::vector<std::size_t>& idxList)
    {
        for (TopExp_Explorer solidExp(shape, TopAbs_SOLID); solidExp.More(); solidExp.Next())
        {
            TopoDS_Solid solid = TopoDS::Solid(solidExp.Current());
            const auto mapIndex = somap_.FindIndex(solid);

            if (mapIndex < 1)
            {
                idxList.push_back(somap_.Add(solid));
                for (TopExp_Explorer shellExp(solid, TopAbs_SHELL); shellExp.More(); shellExp.Next())
                {
                    TopoDS_Shell shell = TopoDS::Shell(shellExp.Current());
                    if (shmap_.FindIndex(shell) < 1)
                    {
                        shmap_.Add(shell);
                        std::vector<std::size_t> dump;
                        addFaces_(shellExp.Current(), dump);
                    }
                }
            }
            else
                idxList.push_back(mapIndex);
        }
    }

    /*!
     * \brief Add faces to the sub-shape maps
     * \param shape The shape of which the faces are to be extracted
     * \param idxList List to which to append the indices of the faces
     *                of the given shape within the local maps.
     */
    void addFaces_(const TopoDS_Shape& shape, std::vector<std::size_t>& idxList)
    {
        for(TopExp_Explorer faceExp(shape, TopAbs_FACE); faceExp.More(); faceExp.Next())
        {
            TopoDS_Face face = TopoDS::Face(faceExp.Current());
            const auto mapIndex = fmap_.FindIndex(face);

            if (mapIndex < 1)
            {
                idxList.push_back(fmap_.Add(face));
                for (TopExp_Explorer wireExp(face.Oriented(TopAbs_FORWARD), TopAbs_WIRE); wireExp.More(); wireExp.Next())
                {
                    TopoDS_Wire wire = TopoDS::Wire(wireExp.Current());
                    if (wmap_.FindIndex(wire) < 1)
                    {
                        wmap_.Add(wire);
                        std::vector<std::size_t> dump;
                        addEdges_(wireExp.Current(), dump);
                    }
                }
            }
            else
                idxList.push_back(mapIndex);
        }
    }

    /*!
     * \brief Add edges to the sub-shape maps
     * \param shape The shape of which the edges are to be extracted
     * \param idxList List to which to append the indices of the edges
     *                of the given shape within the local maps.
     */
    void addEdges_(const TopoDS_Shape& shape, std::vector<std::size_t>& idxList)
    {
        for (TopExp_Explorer edgeExp(shape, TopAbs_EDGE); edgeExp.More(); edgeExp.Next())
        {
            TopoDS_Edge edge = TopoDS::Edge(edgeExp.Current());
            const auto mapIndex = emap_.FindIndex(edge);

            if (mapIndex < 1)
            {
                idxList.push_back(emap_.Add(edge));
                std::vector<std::size_t> dump;
                addVertices_(edgeExp.Current(), dump);
            }
            else
                idxList.push_back(mapIndex);
        }
    }

    /*!
     * \brief Add vertices to the sub-shape maps
     * \param shape The shape of which the vertices are to be extracted
     * \param idxList List to which to append the indices of the vertices
     *                of the given shape within the local maps.
     */
    void addVertices_(const TopoDS_Shape& shape, std::vector<std::size_t>& idxList)
    {
        for (TopExp_Explorer vertExp(shape, TopAbs_VERTEX); vertExp.More(); vertExp.Next())
        {
            TopoDS_Vertex vertex = TopoDS::Vertex(vertExp.Current());
            const auto mapIndex = vmap_.FindIndex(vertex);
            if (mapIndex < 1) idxList.push_back(vmap_.Add(vertex));
            else idxList.push_back(mapIndex);
        }
    }

    /*!
     * \brief Construct the single compound describing the network.
     */
    void makeCompound_()
    {
        BRep_Builder b;
        b.MakeCompound(compound_);

        // use Standard_Integer (as OCC uses this type) instead of
        // std::size_t here to avoid compiler warning
        for(Standard_Integer i = 1; i <= vmap_.Extent(); i++) b.Add(compound_, vmap_(i));
        for(Standard_Integer i = 1; i <= emap_.Extent(); i++) b.Add(compound_, emap_(i));
        for(Standard_Integer i = 1; i <= wmap_.Extent(); i++) b.Add(compound_, wmap_(i));
        for(Standard_Integer i = 1; i <= fmap_.Extent(); i++) b.Add(compound_, fmap_(i));
        for(Standard_Integer i = 1; i <= shmap_.Extent(); i++) b.Add(compound_, shmap_(i));
        for(Standard_Integer i = 1; i <= somap_.Extent(); i++) b.Add(compound_, somap_(i));
    }

    // sub-shape maps
    TopTools_IndexedMapOfShape vmap_, emap_, wmap_, fmap_, shmap_, somap_;

    // single compound describing the overall network
    TopoDS_Compound compound_;

    // index maps from primary to fragment indices
    IndexMapType domainMap_;
    IndexMapType entityMap_;
    IndexMapType intersectionMap_;
    IndexMapType junctionMap_;
};

} // end namespace Frackit

#endif // FRACKIT_BREP_WRITER_HH
