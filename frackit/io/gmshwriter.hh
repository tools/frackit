// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup IO
 * \brief Class that writes entity networks into Gmsh .geo file format.
 *        Effectively, this will write a .brep file and a .geo file in
 *        which the .brep file is included. Then, physical definitions
 *        are added to all entity and sub-domain fragments.
 */
#ifndef FRACKIT_GMSH_WRITER_HH
#define FRACKIT_GMSH_WRITER_HH

#include <cmath>
#include <array>
#include <string>
#include <vector>
#include <fstream>
#include <algorithm>
#include <stdexcept>
#include <type_traits>
#include <unordered_set>
#include <unordered_map>
#include <initializer_list>

#include <frackit/common/id.hh>
#include "brepwriter.hh"
#include "gmshbackend.hh"

namespace Frackit {

/*!
 * \ingroup IO
 * \nosubgrouping
 * \brief Class that writes entity networks into Gmsh .geo file format.
 *        Effectively, this will write a .brep file and a .geo file in
 *        which the .brep file is included. Then, physical geometry and
 *        mesh size definitions are added to all entity and sub-domain
 *        fragments. Several setter functions for mesh size definitions
 *        at physical subdomains or entities are available, where users
 *        have to pass the geometry id(s) and the desired characteristic
 *        mesh size. These ids correspond to the persistend identifiers of
 *        network entities, see the EntityNetwork class documentation.
 */
class GmshWriter : public BRepWriter
{
    using IdMeshSizeMap = std::unordered_map<std::size_t, double>;
    using IdMeshSizeDataPair = typename IdMeshSizeMap::value_type;
    using IndexMapType = typename BRepWriter::IndexMapType;

public:

    //! tags to refer to geometries of the network
    enum class GeometryTag { subDomain, entity, intersection, junction };

    //! return the geometry tag for a given geometry dimension
    GeometryTag getGeometryTag(int geomDim) const
    {
        const auto codim = worldDimension_() - geomDim;
        if (codim == 0) return GeometryTag::subDomain;
        else if (codim == 1) return GeometryTag::entity;
        else if (codim == 2) return GeometryTag::intersection;
        else if (codim == 3) return GeometryTag::junction;
        throw std::runtime_error("Cannot deduce geometry tag from given dimension");
    }

    /*!
     * \brief Constructor from an entity network.
     *        Sets the default gmsh file format version.
     */
    template<class EntityNetwork>
    GmshWriter(const EntityNetwork& network)
    : BRepWriter(network)
    , entityDimension_(network.entityDimension())
    , gmshBackend_()
    {}

    /*!
     * \brief Constructor from an entity network.
     *        Sets the default gmsh file format version.
     */
    template<class EntityNetwork>
    GmshWriter(const EntityNetwork& network,
               int gmshMajorVersion,
               int gmshMinorVersion)
    : BRepWriter(network)
    , entityDimension_(network.entityDimension())
    , gmshBackend_(gmshMajorVersion, gmshMinorVersion)
    {}

    /*!
     * \brief Set gmsh file format version.
     */
    void setGmshVersion(int maj, int min)
    {
        gmshBackend_.setVersion(maj, min);
    }

    /*!
     * \brief Write the .geo file.
     * \param fileName The body of the file name to be used.
     * \note Two files are created: _fileName_.brep and _fileName_.geo
     */
    void write(const std::string& fileName) const
    {
        BRepWriter::write(fileName);

        std::ofstream geoFile(fileName + ".geo");
        geoFile << "Merge \"";
        geoFile << fileName + ".brep";
        geoFile << "\";\n";

        writeGeometries_(geoFile);
        writeMeshSizes_(geoFile);
    }

    /*!
     * \brief Write the .geo file.
     * \param fileName The body of the file name to be used.
     * \param meshSizeAtEntities Characteristic mesh size at entities.
     * \param meshSizeAtBoundary Characteristic mesh size at domain boundaries.
     * \note This interface is deprecated. Please use the setter functions
     *       to define mesh sizes at entites and in subdomains.
     */
    template<class ctype>
    [[deprecated("This interface is deprecated. Use setter functions to define mesh sizes instead.")]]
    void write(const std::string& fileName,
               ctype meshSizeAtEntities,
               ctype meshSizeAtBoundary)
    {
        setMeshSize(GeometryTag::entity, meshSizeAtEntities);
        setMeshSize(GeometryTag::subDomain, meshSizeAtBoundary);
        write(fileName);
    }

    /*!
     * \brief Write the .geo file.
     * \param fileName The body of the file name to be used.
     * \param meshSizeAtEntities Characteristic mesh size at entities.
     * \note This uses the provided mesh size on both entities and boundaries.
     * \note This interface is deprecated. Please use the setter functions
     *       to define mesh sizes at entites and in subdomains.
     */
    template<class ctype>
    [[deprecated("This interface is deprecated. Use setter functions to define mesh sizes instead.")]]
    void write(const std::string& fileName, ctype meshSizeAtEntities)
    { write(fileName, meshSizeAtEntities, meshSizeAtEntities); }


    /** \name Setter functions for the physical status of geometries
     *        Per default, all subdomains, entities and entity intersections
     *        are physical, that means they appear in the computational mesh.
     *        With non-physical geometries, one is able to define mesh sizes
     *        locally around geometric features without these features to be
     *        part of the resulting mesh.
     */
    ///@{

    /*!
     * \brief Define a geometry to be non-physical.
     * \param tag The tag of the geometry type (i.e. subdomain, entity, ...)
     * \param id The physical id of the geometry
     */
    void setNonPhysical(GeometryTag tag, const Id& id);

    /*!
     * \brief Define all geometries of the given type to be non-physical.
     * \param tag The tag of the geometry type (i.e. subdomain, entity, ...)
     */
    void setNonPhysical(GeometryTag tag);

    //! Overload for iterable containers of Id
    template<class IdList>
    void setNonPhysical(GeometryTag tag, const IdList& ids);

    //! Overload for initializer lists
    void setNonPhysical(GeometryTag tag, const std::initializer_list<Id>& ids);

    /*!
     * \brief Define a geometry to be physical.
     * \param tag The tag of the geometry type (i.e. subdomain, entity, intersection)
     * \param id The physical id of the geometry
     */
    void setPhysical(GeometryTag tag, const Id& id);

    /*!
     * \brief Define all geometries of the given type to be physical.
     * \param tag The tag of the geometry type (i.e. subdomain, entity, intersection)
     */
    void setPhysical(GeometryTag tag);

    //! Overload for iterable containers of Id
    template<class IdList>
    void setPhysical(GeometryTag tag, const IdList& ids);

    //! Overload for initializer lists
    void setPhysical(GeometryTag tag, const std::initializer_list<Id>& ids);

    ///@}
    /** \name Setter functions for mesh sizes around geometries
     */
    ///@{

    /*!
     * \brief Set the mesh size to be used on all geometries of the given type.
     * \param tag The tag of the geometry type (i.e. subdomain, entity, intersection)
     * \param meshSize The mesh size to be used on the geometry
     */
    template<class ctype>
    void setMeshSize(GeometryTag tag, ctype meshSize);

    /*!
     * \brief Set the mesh size to be used on a geometry.
     * \param tag The tag of the geometry type (i.e. subdomain, entity, intersection)
     * \param id The physical id of the geometry
     * \param meshSize The mesh size to be used on the geometry
     */
    template<class ctype>
    void setMeshSize(GeometryTag tag, const Id& id, ctype meshSize);

    //! Overload for iterable containers of ids.
    template<class IdList, class ctype>
    void setMeshSize(GeometryTag tag, const IdList& ids, ctype meshSize);

    //! Overload for initializer lists
    template<class ctype>
    void setMeshSize(GeometryTag tag, const std::initializer_list<Id>& ids, ctype meshSize);

    //! Resets all mesh size definitions.
    void resetMeshSizes()
    {
        for (auto& map : meshSize_)
            map.clear();
    }

    //! Resets the mesh size definition for the geometries of the given type.
    void resetMeshSizes(GeometryTag tag)
    {
        meshSize_[codimension_(tag)].clear();
    }

    ///@}

private:
    int networkDimension_() const
    { return entityDimension_; }

    int worldDimension_() const
    { return networkDimension_() + 1; }

    int geometryDimension_(GeometryTag tag) const
    { return worldDimension_() - codimension_(tag); }

    int codimension_(GeometryTag tag) const
    {
        if (tag == GeometryTag::subDomain) return 0;
        else if (tag == GeometryTag::entity) return 1;
        else if (tag == GeometryTag::intersection) return 2;
        else if (tag == GeometryTag::junction) return 3;
        throw std::runtime_error("GeometryTag not supported");
    }

    std::string name_(GeometryTag tag) const
    {
        if (tag == GeometryTag::subDomain) return "subDomain";
        else if (tag == GeometryTag::entity) return "entity";
        else if (tag == GeometryTag::intersection) return "intersection";
        else if (tag == GeometryTag::junction) return "junction";
        throw std::runtime_error("GeometryTag not supported");
    }

    void checkIdValidity_(GeometryTag tag, const Id& id)
    {
        const auto& map = getIdToFragmentsMap_(tag);
        if (map.find(id.get()) == map.end())
            throw std::runtime_error(name_(tag) + " with given id is not defined.");
    }

    template<class ctype>
    void checkMeshSizeValidity_(ctype meshSize) const
    {
        static_assert(std::is_convertible_v<ctype, double>, "Mesh size must be a scalar!");
        if (meshSize <= 0.0) throw std::runtime_error("Mesh sizes must be greater than zero!");
    }

    void writeGeometries_(std::ofstream& geoFile) const
    {
        // lambda to check if a geometry sould be non-physical
        auto isNonPhysical = [&] (int codim, std::size_t id) -> bool
        { return nonPhysicalIds_[codim].find(id) != nonPhysicalIds_[codim].end(); };

        geoFile << "\n"
                << "///////////////////////////////////\n"
                << "// Physical geometry definitions //\n"
                << "///////////////////////////////////\n";

        if (!this->domainToFragmentsMap().empty())
        {
            geoFile << "\n// -- Physical domain definitions\n";
            auto it = this->domainToFragmentsMap().begin();
            for (; it != this->domainToFragmentsMap().end(); ++it)
                if (!isNonPhysical(0, it->first))
                    gmshBackend_.writePhysicalGeometry(geoFile, worldDimension_(),
                                                       it->first, it->second);
        }

        if (!this->entityToFragmentsMap().empty())
        {
            geoFile << "\n// -- Physical entity definitions\n";
            auto it = this->entityToFragmentsMap().begin();
            for (; it != this->entityToFragmentsMap().end(); ++it)
                if (!isNonPhysical(1, it->first))
                    gmshBackend_.writePhysicalGeometry(geoFile, networkDimension_(),
                                                       it->first, it->second);
        }

        if (!this->entityIntersectionsToFragmentsMap().empty())
        {
            geoFile << "\n// -- Physical entity intersection definitions\n";
            auto it = this->entityIntersectionsToFragmentsMap().begin();
            for (; it != this->entityIntersectionsToFragmentsMap().end(); ++it)
                if (!isNonPhysical(2, it->first))
                    gmshBackend_.writePhysicalGeometry(geoFile, networkDimension_()-1,
                                                       it->first, it->second);
        }

        if (!this->junctionToFragmentsMap().empty())
        {
            geoFile << "\n// -- Physical intersection junctions definitions\n";
            auto it = this->junctionToFragmentsMap().begin();
            for (; it != this->junctionToFragmentsMap().end(); ++it)
                if (!isNonPhysical(3, it->first))
                    gmshBackend_.writePhysicalGeometry(geoFile, networkDimension_()-2,
                                                       it->first, it->second);
        }
    }

    void writeMeshSizes_(std::ofstream& geoFile) const
    {
        if (meshSize_[0].size() != this->domainToFragmentsMap().size())
            std::cout << "Warning: there are sub-domains with unspecified mesh size!\n";
        if (meshSize_[1].size() != this->entityToFragmentsMap().size())
            std::cout << "Warning: there are entities with unspecified mesh size!\n";

        const auto data = getSortedMeshSizeData_();
        if (data.empty())
            return;

        geoFile << "\n"
                << "///////////////////////////\n"
                << "// Mesh size definitions //\n"
                << "///////////////////////////\n\n";
        geoFile << "// The order of definition is such that those geometries\n";
        geoFile << "// with the coarsest defined mesh sizes are placed first\n";
        geoFile << "// to ensure that for shared vertices the minimum mesh size is set\n\n";

        // helper struct to define variable names for the occurring mesh sizes
        Gmsh::Detail::MeshSizeVariableNameHelper varNameHelper;

        // first write size variables
        for (const auto& d : data)
            if (varNameHelper.prepareNew(d.first, d.second->second))
                gmshBackend_.writeVariableDefinition(geoFile, varNameHelper.get(d.first), d.second->second);

        // then write the actual size definitions
        geoFile << "\n";
        varNameHelper.reset();
        for (const auto& d : data)
        {
            const auto codim = d.first;
            const auto id = d.second->first;
            const auto size = d.second->second;
            const auto geomDim = worldDimension_() - codim;

            varNameHelper.prepareNew(codim, size);
            geoFile << "// physical " << name_(getGeometryTag(geomDim)) << " " << id << "\n";
            if (codim == 0)
                gmshBackend_.writeMeshSizeDefinition(geoFile, geomDim, this->domainToFragmentsMap().at(id), varNameHelper.get(codim));
            else if (codim == 1)
                gmshBackend_.writeMeshSizeDefinition(geoFile, geomDim, this->entityToFragmentsMap().at(id), varNameHelper.get(codim));
            else if (codim == 2)
                gmshBackend_.writeMeshSizeDefinition(geoFile, geomDim, this->entityIntersectionsToFragmentsMap().at(id), varNameHelper.get(codim));
            else
                gmshBackend_.writeMeshSizeDefinition(geoFile, geomDim, this->junctionToFragmentsMap().at(id), varNameHelper.get(codim));
        }
    }

    std::vector< std::pair<unsigned int,
                           const IdMeshSizeDataPair*> >
    getSortedMeshSizeData_() const
    {
        using Codim = unsigned int;
        using MeshSizeDataPtr = const IdMeshSizeDataPair*;
        using DataPair = std::pair<Codim, MeshSizeDataPtr>;
        using Result = std::vector<DataPair>;
        Result result;

        for (unsigned int codim = 0; codim < meshSize_.size(); ++codim)
            for (const auto& pair : meshSize_[codim])
                result.emplace_back(std::make_pair(codim, &pair));

        // sort such that coarsest mesh definitions come first
        std::sort(result.rbegin(), result.rend(),
                  [] (const auto& p1, const auto& p2)
                  { return p1.second->second < p2.second->second; });

        return result;
    }

    const IndexMapType& getIdToFragmentsMap_(GeometryTag tag)
    {
        if (tag == GeometryTag::subDomain)
            return this->domainToFragmentsMap();
        else if (tag == GeometryTag::entity)
            return this->entityToFragmentsMap();
        else if (tag == GeometryTag::intersection)
            return this->entityIntersectionsToFragmentsMap();
        else if (tag == GeometryTag::junction)
            return this->junctionToFragmentsMap();
        throw std::runtime_error("GeometryTag not supported");
    }

    int entityDimension_;
    Gmsh::GmshFormatBackend gmshBackend_;

    // saves registered mesh sizes, sorted after codimension
    std::array<IdMeshSizeMap, 4> meshSize_;

    // saves registered ids for non-physical entities, sorted after codimension
    std::array<std::unordered_set<std::size_t>, 4> nonPhysicalIds_;
};

///////////////////////////////////////////////////
// setter function definitions for physical status
void GmshWriter::setNonPhysical(GmshWriter::GeometryTag tag, const Id& id)
{
    checkIdValidity_(tag, id);
    nonPhysicalIds_[codimension_(tag)].insert(id.get());
}

void GmshWriter::setNonPhysical(GmshWriter::GeometryTag tag)
{
    for (const auto& [id, fragmentList] : getIdToFragmentsMap_(tag))
        setNonPhysical(tag, Id(id));
}

template<class IdList>
void GmshWriter::setNonPhysical(GmshWriter::GeometryTag tag, const IdList& ids)
{ for (const auto& id : ids) setNonPhysical(tag, id); }

void GmshWriter::setNonPhysical(GmshWriter::GeometryTag tag, const std::initializer_list<Id>& ids)
{ for (const auto& id : ids) setNonPhysical(tag, id); }

void GmshWriter::setPhysical(GmshWriter::GeometryTag tag, const Id& id)
{
    checkIdValidity_(tag, id);
    auto it = nonPhysicalIds_[codimension_(tag)].find(id.get());
    if (it != nonPhysicalIds_[codimension_(tag)].end())
        nonPhysicalIds_[codimension_(tag)].erase(it);
}

void GmshWriter::setPhysical(GmshWriter::GeometryTag tag)
{
    for (const auto& [id, fragmentList] : getIdToFragmentsMap_(tag))
        setPhysical(tag, Id(id));
}

template<class IdList>
void GmshWriter::setPhysical(GmshWriter::GeometryTag tag, const IdList& ids)
{ for (const auto& id : ids) setPhysical(tag, id); }

void GmshWriter::setPhysical(GmshWriter::GeometryTag tag, const std::initializer_list<Id>& ids)
{ for (const auto& id : ids) setPhysical(tag, id); }

//////////////////////////////////////////////
// setter functions for mesh size definitions
template<class ctype>
void GmshWriter::setMeshSize(GmshWriter::GeometryTag tag, ctype meshSize)
{
    for (const auto& [id, fragmentList] : getIdToFragmentsMap_(tag))
        setMeshSize(tag, Id(id), meshSize);
}

template<class ctype>
void GmshWriter::setMeshSize(GmshWriter::GeometryTag tag, const Id& id, ctype meshSize)
{
    checkIdValidity_(tag, id);
    checkMeshSizeValidity_(meshSize);
    meshSize_[codimension_(tag)][id.get()] = meshSize;
}

template<class IdList, class ctype>
void GmshWriter::setMeshSize(GmshWriter::GeometryTag tag, const IdList& ids, ctype meshSize)
{ for (const auto& id : ids) setMeshSize(tag, id, meshSize); }

template<class ctype>
void GmshWriter::setMeshSize(GmshWriter::GeometryTag tag, const std::initializer_list<Id>& ids, ctype meshSize)
{ for (const auto& id : ids) setMeshSize(tag, id, meshSize); }

} // end namespace Frackit

#endif // FRACKIT_GMSH_WRITER_HH
