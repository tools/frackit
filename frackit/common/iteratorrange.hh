// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Common
 * \brief Class that represents a range between begin and end iterators.
 *        Can be used to support range-based for loops.
 *        The implementation is inspired by the one provided in Dune, see
 *        https://gitlab.dune-project.org/core/dune-common/-/blob/master/dune/common/iteratorrange.hh
 */
#ifndef FRACKIT_COMMON_ITERATORRANGE_HH
#define FRACKIT_COMMON_ITERATORRANGE_HH

namespace Frackit {

/*!
 * \ingroup Common
 * \brief Class that represents a range between begin and end iterators.
 *        Can be used to support range-based for loops.
 *        The implementation is inspired by the one provided in Dune, see
 *        https://gitlab.dune-project.org/core/dune-common/-/blob/master/dune/common/iteratorrange.hh
 */
template<typename Iterator>
class IteratorRange
{

public:
    using iterator = Iterator;
    using const_iterator = Iterator;

    IteratorRange() = default;
    IteratorRange(const Iterator& begin, const Iterator& end)
    : begin_(begin) , end_(end) {}

    Iterator begin() const { return begin_; }
    Iterator end() const { return end_; }

private:
    Iterator begin_;
    Iterator end_;
};

} // end namespace Frackit

#endif // FRACKIT_COMMON_ITERATORRANGE_HH
