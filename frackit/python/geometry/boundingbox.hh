// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef FRACKIT_PYTHON_GEOMETRY_BOUNDING_BOX_HH
#define FRACKIT_PYTHON_GEOMETRY_BOUNDING_BOX_HH

#include <pybind11/pybind11.h>
#include <pybind11/operators.h>

#include <frackit/geometry/box.hh>
#include <frackit/geometry/boundingbox.hh>
#include "registerdimensionproperties.hh"

namespace Frackit::Python {

namespace py = pybind11;

template<class ctype>
void registerBoundingBox(py::module& module)
{
    using namespace py::literals;
    using Box = Box<ctype>;
    using BoundingBox = BoundingBox<ctype>;
    using Point = typename BoundingBox::Point;

    // define constructors
    py::class_<BoundingBox, Box> cls(module, "BoundingBox");
    cls.def(py::init<ctype, ctype, ctype, ctype, ctype, ctype>(),
            "xmin"_a, "ymin"_a, "zmin"_a, "xmax"_a, "ymax"_a, "zmax"_a);
    cls.def(py::init<const Point&, const Point&>(), "firstCorner"_a, "secondCorner"_a);
    cls.def(py::init<const Box&>(), "box"_a);
    cls.def(py::init<>());

    // dimensionality properties
    registerDimensionProperties(cls);

    cls.def("name", &BoundingBox::name, "name of the geometry class");
    cls.def("__repr__", [&] (const BoundingBox& b) { return "Frackit::BoundingBox"; });
    cls.def(py::self += BoundingBox());
    cls.def(py::self + BoundingBox());
}

} // end namespace Frackit::Python

#endif
