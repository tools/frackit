// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef FRACKIT_PYTHON_IO_GMSH_WRITER_HH
#define FRACKIT_PYTHON_IO_GMSH_WRITER_HH

#include <vector>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include <frackit/io/gmshwriter.hh>
#include <frackit/common/id.hh>
#include <frackit/entitynetwork/entitynetwork.hh>
#include <frackit/python/entitynetwork/traits.hh>

namespace Frackit::Python {

namespace py = pybind11;

template<class ctype>
void registerGmshWriter(py::module& module)
{
    using EntityNetwork = Frackit::EntityNetwork<PythonEntityNetworkTraits>;

    py::class_<GmshWriter> cls(module, "GmshWriter");
    cls.def(py::init<const EntityNetwork&>());
    cls.def(py::init<const EntityNetwork&, int, int>());

    using namespace py::literals;

    // overloading with py::overload_cast seemed to fail when having mixed template/non-template functions
    cls.def("write",
            static_cast<void (GmshWriter::*)(const std::string&) const>(&GmshWriter::write),
            "fileName"_a, "write the entity network to a .geo file");

    // register geometry tags for setter functions
    py::enum_<GmshWriter::GeometryTag>(cls, "GeometryTag")
    .value("subDomain", GmshWriter::GeometryTag::subDomain)
    .value("entity", GmshWriter::GeometryTag::entity)
    .value("intersection", GmshWriter::GeometryTag::intersection)
    .value("junction", GmshWriter::GeometryTag::junction);

    // register geometry tag getter function for given dimension
    cls.def("getGeometryTag", &GmshWriter::getGeometryTag,
            "returns the geometry tag associated with the geometries of the given dimension");

    // physical status setter functions
    cls.def("setGmshVersion", &GmshWriter::setGmshVersion, "set gmsh format version");

    // we define overloads for vector<Id> which is automatically converted
    // by pybind11 such that it can be used with lists from within Python
    using IdList = std::vector<Id>;

    cls.def("setNonPhysical",
            static_cast<void (GmshWriter::*)(GmshWriter::GeometryTag)>(&GmshWriter::setNonPhysical),
            "set all geometries with the given type tag to be non-physical");
    cls.def("setNonPhysical",
            static_cast<void (GmshWriter::*)(GmshWriter::GeometryTag, const Id&)>(&GmshWriter::setNonPhysical),
            "set the geometry with the given type tag and identifier to be non-physical");
    cls.def("setNonPhysical",
            py::overload_cast<GmshWriter::GeometryTag, const IdList&>(&GmshWriter::template setNonPhysical<IdList>),
            "set all geometries with a specific type tag and with the ids of the given list to be non-physical");
    cls.def("setPhysical",
            static_cast<void (GmshWriter::*)(GmshWriter::GeometryTag)>(&GmshWriter::setPhysical),
            "set all geometries with the given type tag to be physical");
    cls.def("setPhysical",
            static_cast<void (GmshWriter::*)(GmshWriter::GeometryTag, const Id&)>(&GmshWriter::setPhysical),
            "set the geometry with the given type tag and identifier to be physical");
    cls.def("setPhysical",
            py::overload_cast<GmshWriter::GeometryTag, const IdList&>(&GmshWriter::template setPhysical<IdList>),
            "set all geometries with a specific type tag and with the ids of the given list to be physical");

    // mesh size setters
    cls.def("setMeshSize",
            static_cast<void (GmshWriter::*)(GmshWriter::GeometryTag, ctype)>(&GmshWriter::template setMeshSize<ctype>),
            "set the mesh size for all geometries with the given type tag");
    cls.def("setMeshSize",
            static_cast<void (GmshWriter::*)(GmshWriter::GeometryTag, const Id&, ctype)>(&GmshWriter::template setMeshSize<ctype>),
            "set the mesh size for the geometry with the given type tag and identifier");
    cls.def("setMeshSize",
            py::overload_cast<GmshWriter::GeometryTag, const IdList&, ctype>(&GmshWriter::template setMeshSize<IdList, ctype>),
            "set the mesh size for all geometries with a specific type tag and with the ids of the given list");
    cls.def("resetMeshSizes",
            py::overload_cast<>(&GmshWriter::resetMeshSizes),
            "Reset all mesh size settings");
    cls.def("resetMeshSizes",
            py::overload_cast<GmshWriter::GeometryTag>(&GmshWriter::resetMeshSizes),
            "Reset the mesh size settings for geometries with the given type tag");

    // deprecated (TODO: remove afer 1.2 release)
    cls.def("write",
            py::overload_cast<const std::string&, ctype, ctype>(&GmshWriter::template write<ctype>),
            "fileName"_a, "meshSizeAtEntities"_a, "meshSizeAtBoundary"_a,
            "write the entity network to a .geo file");
    cls.def("write",
            py::overload_cast<const std::string&, ctype>(&GmshWriter::template write<ctype>),
            "fileName"_a, "meshSizeAtEntities"_a,
            "write the entity network to a .geo file");
}

} // end namespace Frackit::Python

#endif
