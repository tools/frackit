// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef FRACKIT_PYTHON_ENTITY_NETWORK_CONSTITUENTS_HH
#define FRACKIT_PYTHON_ENTITY_NETWORK_CONSTITUENTS_HH

#include <pybind11/pybind11.h>

#include <TopoDS_Shape.hxx>
#include <frackit/python/geometry/brepwrapper.hh>

namespace Frackit::Python {
namespace EntityNetworkConstituents {

class Constituent
{
    using OCCShape = TopoDS_Shape;

public:
    using Shape = ShapeWrapper;

    Constituent(const Shape& s, std::size_t idx) : shape_(s) , index_(idx) {}
    Constituent(const OCCShape& s, std::size_t idx) : shape_(s) , index_(idx) {}

    const Shape& shape() const { return shape_; }
    std::size_t index() const { return index_; }

private:
    Shape shape_;
    std::size_t index_;
};

class SubDomainFragment : public Constituent { public: using Constituent::Constituent; };
class EntityFragment : public Constituent { public: using Constituent::Constituent; };
class IntersectionFragment : public Constituent { public: using Constituent::Constituent; };
class IntersectionJunction : public Constituent { public: using Constituent::Constituent; };

} // end namespace EntityNetworkConstituents

namespace py = pybind11;
void registerConstituents(py::module& module)
{
    using Base = Frackit::Python::EntityNetworkConstituents::Constituent;
    using Domain = Frackit::Python::EntityNetworkConstituents::SubDomainFragment;
    using Entity = Frackit::Python::EntityNetworkConstituents::EntityFragment;
    using Intersection = Frackit::Python::EntityNetworkConstituents::IntersectionFragment;
    using Junction = Frackit::Python::EntityNetworkConstituents::IntersectionJunction;

    py::class_<Base> baseConstituent(module, "BaseConstituent");
    baseConstituent.def("shape", &Base::shape, "returns the shape of the constituent");
    baseConstituent.def("index", &Base::index, "returns the index of the constituent within the network");
    py::class_<Domain, Base>(module, "DomainFragment");
    py::class_<Entity, Base>(module, "EntityFragment");
    py::class_<Intersection, Base>(module, "IntersectionFragment");
    py::class_<Junction, Base>(module, "IntersectionJunction");
}

} // end namespace Frackit::Python

#endif // FRACKIT_PYTHON_sENTITY_NETWORK_CONSTITUENTS_HH
