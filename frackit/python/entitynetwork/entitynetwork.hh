// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef FRACKIT_PYTHON_ENTITY_NETWORK_HH
#define FRACKIT_PYTHON_ENTITY_NETWORK_HH

#include <pybind11/pybind11.h>

#include <frackit/common/id.hh>
#include <frackit/entitynetwork/entitynetwork.hh>
#include "traits.hh"

namespace Frackit::Python {

namespace py = pybind11;

void registerEntityNetworks(py::module& module)
{
    registerConstituents(module);

    using EN = EntityNetwork<PythonEntityNetworkTraits>;
    py::class_<EN> en(module, "EntityNetwork");
    en.def("entityDimension", &EN::entityDimension, "return the dimension of the network entities");
    en.def("domainDimension", &EN::domainDimension, "return the dimension of the embedding domain (if defined)");
    en.def("isContained", &EN::isContained, "returns true if the domain is contained in a domain");

    // return functions for network size
    en.def("numSubDomains", &EN::numSubDomains, "returns the number of primary subdomains");
    en.def("numSubDomainFragments", &EN::numSubDomainFragments, "returns the number of all subdomain fragments");
    en.def("numEntities", &EN::numEntities, "returns the number of primary entities");
    en.def("numEntityFragments", &EN::numEntityFragments, "returns the number of all entity fragments");
    en.def("numIntersections", &EN::numIntersections, "returns the number of primary entity intersections");
    en.def("numIntersectionFragments", &EN::numIntersectionFragments, "returns the number of all entity intersection fragments");
    en.def("numIntersectionJunctions", &EN::numIntersectionJunctions, "returns the number of intersection junctions");

    // return functions for primary ids
    en.def("subDomainId", &EN::subDomainId, "returns the primary subdomain id the given fragment belongs to");
    en.def("entityId", &EN::entityId, "returns the primary entity id the given fragment belongs to");
    en.def("intersectionId", &EN::intersectionId, "returns the primary entity intersection id the given fragment belongs to");
    en.def("intersectionJunctionId", &EN::intersectionJunctionId, "returns the primary intersection junction id for the given junction");
}

} // end namespace Frackit::Python

#endif
