// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Geometry
 * \brief Class that implements axis-aligned boxes in 3d space.
 */
#ifndef FRACKIT_GEOMETRY_BOUNDING_BOX_HH
#define FRACKIT_GEOMETRY_BOUNDING_BOX_HH

#include <array>
#include <cmath>

#include "box.hh"

namespace Frackit {

/*!
 * \ingroup Geometry
 * \brief Class that implements axis-aligned boxes in 3d space.
 * \tparam CT The type used for coordinates
 * \note This class is different from the Box class in the sense that it
 *       is guaranteed that the box is axis-aligned.
 */
template<class CT>
class BoundingBox : public Box<CT>
{
    using ThisType = BoundingBox<CT>;
    using ParentType = Box<CT>;

public:
    //! pull up base's constructors
    using ParentType::ParentType;

    //! construction from a box
    BoundingBox(const Box<CT>& box)
    : ParentType(box)
    {}

    //! default constructor (zero volume bounding box)
    BoundingBox()
    : ParentType(0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
    {}

    /*!
     * \brief Merge bounding box with another.
     */
    BoundingBox<CT>& operator+=(const BoundingBox<CT>& other)
    {
        const auto coords = getMergedCoords(other);
        this->setXMin(coords[0][0]); this->setXMax(coords[0][1]);
        this->setYMin(coords[1][0]); this->setYMax(coords[1][1]);
        this->setZMin(coords[2][0]); this->setZMax(coords[2][1]);
        return *this;
    }

    /*!
     * \brief Merge bounding box with another.
     */
    BoundingBox<CT> operator+(const BoundingBox<CT>& other) const
    {
        const auto coords = getMergedCoords(other);
        return BoundingBox<CT>(coords[0][0], coords[1][0], coords[2][0],
                               coords[0][1], coords[1][1], coords[2][1]);
    }

    //! Return the name of this geometry.
    std::string name() const override { return "BoundingBox"; }

private:
    //! returns the coordinates after merge with other bounding box
    std::array< std::array<CT, 2>, 3>
    getMergedCoords(const BoundingBox<CT>& other) const
    {
        using std::min; using std::max;

        std::array<CT, 2> xCoords;
        xCoords[0] = min(this->xMin(), other.xMin());
        xCoords[1] = max(this->xMax(), other.xMax());

        std::array<CT, 2> yCoords;
        yCoords[0] = min(this->yMin(), other.yMin());
        yCoords[1] = max(this->yMax(), other.yMax());

        std::array<CT, 2> zCoords;
        zCoords[0] = min(this->zMin(), other.zMin());
        zCoords[1] = max(this->zMax(), other.zMax());

        return {std::move(xCoords), std::move(yCoords), std::move(zCoords)};
    }
};

} // end namespace Frackit

#endif // FRACKIT_GEOMETRY_BOUNDING_BOX_HH
