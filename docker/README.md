# Run Frackit inside docker

If you have [docker](https://www.docker.com/) installed on your system, you can
use the image hosted in this repository to run Frackit inside a docker container
without the need to install anything on your system. Type

```sh
docker run -it git.iws.uni-stuttgart.de:4567/tools/frackit/ubuntu18.04:latest
```

to start a terminal inside the docker container interactively (`-it`). This will
bring you to the folder `/frackit` inside the container. Therein, you can find a
convenience script `install_or_update.sh`, which installs Frackit in-place
(or updates it in case you had run the script already before). The script also
builds the python bindings for you to use anywhere inside the container and it
precompiles all examples. To run the precomiled examples, for instance `example1`,
go to the respective folder in the build tree and run the example by typing

```sh
cd frackit/build/appl/example1
./example1
```

into the terminal.

## Using a shared folder

The docker container is set up for you to define a shared folder in `/frackit/shared`.
For example, let us define the folder `/home/username/tmp` on the host machine
to be shared with the container. To realize this, start the container with the
`-v` flag as follows:

```sh
docker run -it -v /home/username/tmp:/frackit/shared git.iws.uni-stuttgart.de:4567/tools/frackit/ubuntu18.04:latest
```

Any data that you now place within `/home/username/tmp` on the host machine will
be accessible, although read-only (!), from within the container inside the folder `/frackit/shared`.
In the sequel, it will be explained how to achieve read and write access to the data in the shared
folder from both the host machine and from within the container.

Upon startup, a helper script is executed that manages the permissions for the
shared folder in `/frackit/shared` inside the container. In order to setup the
shared folder such that you have read and write permission both from within the
container as well as from the host machine, define the environment variables
`HOST_UID` and `HOST_GID` by using the following command to startup the container:

```sh
docker run -it -v /home/username/tmp:/frackit/shared -e HOST_UID="$(id -u ${USER})" -e HOST_GID="$(id -g ${USER})" git.iws.uni-stuttgart.de:4567/tools/frackit/ubuntu18.04:latest
```

This will internally set the user and group ids of the frackit user inside the container
to those of the user of the host machine. In the above command, "$(id -u ${USER})" and
"$(id -g ${USER})" are Linux commands that are used to extract the ids of the user that is
starting the container on the host machine. If you know your ids you can also pass them
directly. In particular, you should substitute these if you are running the container
from Windows (execution from windows has not been tested yet).
