#!/bin/bash

# Convenience script for users to install or update frackit.
# You can pass the opencascade version you would like to use,
# e.g., by calling ./install_or_update.sh -occ 7.3. Default is version 7.3.
OCC_VERSION="7.3"
if [ "$1" == "-occ" ]; then
    OCC_VERSION="$2"
elif [ $# -gt 0 ]; then
    echo "WARNING: Could not parse command line arguments. Falling back to defaults."
fi

OCC_INSTALL_DIR="/home/opencascade-${OCC_VERSION}/install"
if [ -d "${OCC_INSTALL_DIR}" ]; then
    echo "Using OpenCASCADE version ${OCC_VERSION}."
else
    echo "Could not find OpenCASCADE installation at ${OCC_INSTALL_DIR}."
    echo "If you are trying to use a manually installed version, please adjust"
    echo "the installation path in this script accordingly."
    exit 1
fi

# if folder exists already, do not clone
if [ -d "$(pwd)/frackit" ]; then
    cd frackit
else
    git clone https://git.iws.uni-stuttgart.de/tools/frackit.git
    cd frackit
fi

# enter build directory
if [ -d "build" ]; then
    if git pull --rebase origin master; then
        echo "Sucessfully updated master branch. If you are currently working"
        echo "on another branch you should consider rebasing or switching to master."
    else
        echo "Could not update master branch. Do you have incompatible local changes?"
        exit 1
    fi
    cd build
else
    mkdir build
    cd build
fi

echo ""
echo "######################"
echo "# Configuring project"
echo "######################"
echo ""
rm -rf CMakeFiles CMakeCache.txt
cmake -DFRACKIT_CONFIG_FOR_DOCKER=1 -DOCC_INSTALL_DIR="${OCC_INSTALL_DIR}" ../

# install python bindings & precompile example applications
echo ""
echo "############################"
echo "# Installing python bindings"
echo "############################"
echo ""
make && make install_python

echo ""
echo "###################################"
echo "# Precompiling example applications"
echo "###################################"
echo ""
make build_example_applications

echo ""
echo "Frackit and the example applications have been installed successfully."
echo "To run the first example, go to /home/frackit/build/appl/example1"
echo "and type ./example1 or python example1.py to run it"
