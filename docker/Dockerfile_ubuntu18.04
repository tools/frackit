# Dockerfile for building the basic requirements for Frackit, without OpenCascade
# Other dockerfiles handle the installation of OpenCascade using different versions

# see https://github.com/phusion/baseimage-docker for information on the base image
# It is Ubuntu LTS customized for better Docker compatibility
FROM phusion/baseimage:18.04-1.0.0
MAINTAINER dennis.glaeser@iws.uni-stuttgart.de

# run Ubuntu update as advised on https://github.com/phusion/baseimage-docker
RUN apt-get update \
    && apt-get upgrade -y -o Dpkg::Options::="--force-confold" \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# install the basic dependencies
RUN apt-get update \
    && apt-get install --no-install-recommends --yes \
    ca-certificates \
    vim \
    build-essential \
    gcc-7 g++-7 \
    clang-8 clang-10 \
    clang \
    python3-dev \
    python3-pip \
    git \
    pkg-config \
    wget \
    cmake \
    gmsh \
    paraview \
    doxygen \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# install gcc-9
RUN add-apt-repository ppa:ubuntu-toolchain-r/test \
    && apt-get update \
    && apt-get install --yes gcc-9 g++-9 \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# install setup tools & pytest for installation python bindings
RUN python3 -m pip install setuptools
RUN python3 -m pip install pytest
RUN python3 -m pip install gcovr six numpy # required for coverage reports

# install pybind11-2.6 for python bindings
WORKDIR /home
RUN git clone -b v2.6 https://github.com/pybind/pybind11.git
WORKDIR /home/pybind11
RUN mkdir build
WORKDIR /home/pybind11/build
RUN cmake ../ && make && make install

# extract OpenCascade 7.3 & 7.5 into container
# TODO: This expects the tarballs to present in the
#       scope of the docker build and with the given
#       file names. It seems that occ precents automatic
#       retrieval of snapshots with wget or curl from their portal at
#       http://git.dev.opencascade.org/gitweb/?p=occt.git;a=summary
# TODO: Moreover, the folder names should be deduced automatically and
#       changed accordingly.
WORKDIR /home
COPY occ_7_3_0.tar.gz .
RUN tar -xf occ_7_3_0.tar.gz && rm *.tar.gz
RUN mv /home/occt-42da0d5 /home/opencascade-7.3

COPY occ_7_5_0.tar.gz .
RUN tar -xf occ_7_5_0.tar.gz && rm *.tar.gz
RUN mv /home/occt-628c021 /home/opencascade-7.5

# install both libraries
WORKDIR /home/opencascade-7.3/
RUN mkdir build
RUN mkdir install

WORKDIR /home/opencascade-7.3/build
RUN cmake -DBUILD_MODULE_ApplicationFramework=0 \
          -DBUILD_MODULE_DataExchange=0 \
          -DBUILD_MODULE_Draw=0 \
          -DBUILD_MODULE_Visualization=0 \
          -DINSTALL_DIR=/home/opencascade-7.3/install \
          -DCMAKE_INSTALL_RPATH=/home/opencascade-7.3/install/lib \
          ../

RUN make -j 4
RUN make install

WORKDIR /home/opencascade-7.5/
RUN mkdir build
RUN mkdir install

WORKDIR /home/opencascade-7.5/build
RUN cmake -DBUILD_MODULE_ApplicationFramework=0 \
          -DBUILD_MODULE_DataExchange=0 \
          -DBUILD_MODULE_Draw=0 \
          -DBUILD_MODULE_Visualization=0 \
          -DINSTALL_DIR=/home/opencascade-7.5/install \
          -DCMAKE_INSTALL_RPATH=/home/opencascade-7.5/install/lib \
          ../
RUN make -j 4
RUN make install

# define frackit user and group
RUN groupadd -r frackit && useradd -r -g frackit -m -d /frackit frackit

# copy convenience install/update script
WORKDIR /frackit
COPY install_or_update.sh .
RUN chmod +x install_or_update.sh
RUN chown frackit install_or_update.sh

# copy the permission helper script to set permissions for shared folder
COPY setpermissions.sh /etc/my_init.d/setpermissions.sh
RUN chmod +x /etc/my_init.d/setpermissions.sh

# switch to frackit user and define shared folder
USER frackit
VOLUME /home/frackit/shared

# switch back to root
USER root

# set entry point like advised https://github.com/phusion/baseimage-docker
ENTRYPOINT ["/sbin/my_init", "--", "/sbin/setuser", "frackit"]

# per default, start interactive shell
CMD ["/bin/bash","-i"]
