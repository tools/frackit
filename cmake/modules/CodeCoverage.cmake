#  CMake support for code coverage reports with gcov/lcov
#
# Adapted from
# https://github.com/bilke/cmake-modules/blob/master/CodeCoverage.cmake
#
# Copyright (c) 2012 - 2017, Lars Bilke
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# USAGE:
#
# 1. Copy this file into your cmake modules path.
#
# 2. Add the following line to your CMakeLists.txt (best inside an if-condition
#    using a CMake option() to enable it just optionally):
#      include(CodeCoverage)
#
# 3. Append necessary compiler flags:
#      append_coverage_compiler_flags()
#
# 3.a (OPTIONAL) Set appropriate optimization flags, e.g. -O0, -O1 or -Og
#
# 4. If you need to exclude additional directories from the report, specify them
#    using full paths in the COVERAGE_EXCLUDES variable before calling
#    setup_target_for_coverage_*().
#    Example:
#      set(COVERAGE_EXCLUDES
#          '${PROJECT_SOURCE_DIR}/src/dir1/*'
#          '/path/to/my/src/dir2/*')
#    Or, use the EXCLUDE argument to setup_target_for_coverage_*().
#    Example:
#      setup_target_for_coverage_lcov(
#          NAME coverage
#          EXECUTABLE testrunner
#          EXCLUDE "${PROJECT_SOURCE_DIR}/src/dir1/*" "/path/to/my/src/dir2/*")
#
# 4.a NOTE: With CMake 3.4+, COVERAGE_EXCLUDES or EXCLUDE can also be set
#     relative to the BASE_DIRECTORY (default: PROJECT_SOURCE_DIR)
#     Example:
#       set(COVERAGE_EXCLUDES "dir1/*")
#       setup_target_for_coverage_gcovr_html(
#           NAME coverage
#           EXECUTABLE testrunner
#           BASE_DIRECTORY "${PROJECT_SOURCE_DIR}/src"
#           EXCLUDE "dir2/*")
#
# 5. Use the functions described below to create a custom make target which
#    runs your test executable and produces a code coverage report.
#
# 6. Build a Debug build:
#      cmake -DCMAKE_BUILD_TYPE=Debug ..
#      make
#      make my_coverage_target
#

include(CMakeParseArguments)

# find compiler-specific gcov
find_program( GCOVR_PATH gcovr )
get_filename_component(GCC_PATH ${CMAKE_CXX_COMPILER} PATH)
string(REGEX MATCH "^[0-9]+" GCC_MAJOR_VERSION ${CMAKE_CXX_COMPILER_VERSION})
find_program(GCOV_PATH NAMES gcov-${GCC_MAJOR_VERSION} HINTS ${GXX_PATH})
if (GCOV_PATH)
    message(STATUS "Found gcov for c++ compiler version: ${CMAKE_CXX_COMPILER_VERSION} at ${GCOV_PATH}")
endif ()

if (NOT CMAKE_COMPILER_IS_GNUCXX)
    if ("${CMAKE_Fortran_COMPILER_ID}" MATCHES "[Ff]lang")
        # Do nothing; exit conditional without error if true
    elseif ("${CMAKE_Fortran_COMPILER_ID}" MATCHES "GNU")
        # Do nothing; exit conditional without error if true
    else ()
        message(FATAL_ERROR "Compiler is not GNU gcc! Aborting...")
    endif ()
endif ()

# compiler flags specific to coverage reporting for appending to compiler flags
set(ADDITIONAL_COVERAGE_COMPILER_FLAGS "-coverage -fprofile-arcs -ftest-coverage -fPIC")
if (NOT CMAKE_BUILD_TYPE STREQUAL "Debug")
    message(WARNING "Code coverage results with an optimised (non-Debug) build may be misleading")
endif ()

if (CMAKE_C_COMPILER_ID STREQUAL "GNU" OR CMAKE_Fortran_COMPILER_ID STREQUAL "GNU")
    link_libraries(gcov)
endif ()

# Defines a target for running and collection code coverage information
# Builds dependencies, runs the given executable and outputs reports.
# NOTE! The executable should always have a ZERO as exit code otherwise
# the coverage generation will not complete.
#
# frackit_setup_target_for_coverage_xml(
#     NAME ctest_coverage                    # New target name
#     EXECUTABLE ctest -j ${PROCESSOR_COUNT} # Executable in PROJECT_BINARY_DIR
#     DEPENDENCIES executable_target         # Dependencies to build first
#     BASE_DIRECTORY "../"                   # Base directory for report
#                                            #  (defaults to PROJECT_SOURCE_DIR)
#     EXCLUDE "src/dir1/*" "src/dir2/*"      # Patterns to exclude (can be relative
#                                            #  to BASE_DIRECTORY, with CMake 3.4+)
# )
function(frackit_setup_target_for_coverage_xml)

    set(options NONE)
    set(oneValueArgs NAME)
    set(multiValueArgs CMD_ARGS)
    cmake_parse_arguments(COVERAGE "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

    if (NOT GCOVR_PATH)
        message(FATAL_ERROR "gcovr not found! Aborting...")
    endif ()

    # Set base directory
    set(GCOVR_ROOT_PATH ${CMAKE_SOURCE_DIR})

    add_custom_target(${COVERAGE_NAME}
        # create folder for cobertura report
        COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_BINARY_DIR}/gcovr

        # Running gcovr (exclude tests/examples & python bindings)
        COMMAND ${GCOVR_PATH} --print-summary
                              -r ${GCOVR_ROOT_PATH}
                              -e ".*test\/.*\.cc"
                              -e ".*example\/.*\.cc"
                              -e ${GCOVR_ROOT_PATH}/python
                              --gcov-executable=${GCOV_PATH}
                              --object-directory=${CMAKE_BINARY_DIR}
                              --exclude-directories ${CMAKE_BINARY_DIR}/python/
                              --xml ${CMAKE_BINARY_DIR}/gcovr/${COVERAGE_NAME}.xml
                              --html ${CMAKE_BINARY_DIR}/gcovr/${COVERAGE_NAME}.html
                              --exclude-unreachable-branches
                              --exclude-throw-branches
                              --sort-uncovered
                              --xml-pretty
                              ${CMD_ARGS}
        WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
        VERBATIM # Protect arguments to commands
        COMMENT "Running ${GCOV_PATH} to produce Cobertura code coverage report."
    )

    # Show info where to find the report
    add_custom_command(TARGET ${COVERAGE_NAME} POST_BUILD
        COMMAND ;
        COMMENT "Cobertura code coverage report saved in ${CMAKE_BINARY_DIR}/gcovr/${COVERAGE_NAME}.xml."
        COMMENT "HTML version saved in ${CMAKE_BINARY_DIR}/public/index.html."
    )
endfunction ()

function(frackit_append_coverage_flags)
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${ADDITIONAL_COVERAGE_COMPILER_FLAGS}" PARENT_SCOPE)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${ADDITIONAL_COVERAGE_COMPILER_FLAGS}" PARENT_SCOPE)
    set(CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} ${ADDITIONAL_COVERAGE_COMPILER_FLAGS}" PARENT_SCOPE)
    message(STATUS "Appending code coverage compiler flags: ${ADDITIONAL_COVERAGE_COMPILER_FLAGS}")
endfunction ()
