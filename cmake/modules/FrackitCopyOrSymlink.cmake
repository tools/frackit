
# function to create a symbolic link between files in the source and build directories
function(frackit_link_from_build_dir)

  # skip this if source and binary dir are the same
  if( ${CMAKE_SOURCE_DIR} STREQUAL ${CMAKE_BINARY_DIR} )
    return()
  endif()

  # parse arguments
  include(CMakeParseArguments)
  cmake_parse_arguments(ARG "" "DESTINATION" "FILES" ${ARGN})
  if (ARG_UNPARSED_ARGUMENTS)
    message(WARNING "Unparsed arguments to frackit_link_from_build_dir)!")
  endif ()

  # handle files
  foreach (f ${ARG_FILES})
    if (ARG_DESTINATION)
      set(destination "${ARG_DESTINATION}")
    else ()
      set(destination "")
    endif ()

    # On windows, make copy
    if (${CMAKE_SYSTEM_NAME} STREQUAL "Windows")
      if(NOT DEFINED FRACKIT_SYMLINK_WARNING)
        message(WARNING "Doing copy instead of symlink!")
        set(FRACKIT_SYMLINK_WARNING)
      endif()
      execute_process(COMMAND ${CMAKE_COMMAND} "-E" "copy" "${CMAKE_CURRENT_SOURCE_DIR}/${f}" "${CMAKE_CURRENT_BINARY_DIR}/${destination}/${f}")
    else ()
      execute_process(COMMAND ${CMAKE_COMMAND} "-E" "create_symlink" "${CMAKE_CURRENT_SOURCE_DIR}/${f}" "${CMAKE_CURRENT_BINARY_DIR}/${destination}/${f}")
    endif ()
  endforeach ()

endfunction(frackit_link_from_build_dir)

function(frackit_symlink_or_copy)
    if(NOT DEFINED FRACKIT_SYMLINK_DEPR_WARNING)
      message(WARNING "This function is deprecated. Use frackit_link_from_build_dir instead!")
      set(FRACKIT_SYMLINK_DEPR_WARNING)
    endif()
    frackit_link_from_build_dir(${ARGV})
endfunction(frackit_symlink_or_copy)
