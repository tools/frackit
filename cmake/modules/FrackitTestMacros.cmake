# The code in this file has been inspired by the dune-common
# module of the dune project (https://gitlab.dune-project.org/core/dune-common),
# in particular from the following file:
# https://gitlab.dune-project.org/core/dune-common/-/blob/master/cmake/modules/DuneTestMacros.cmake

# enable the testing suite on the CMake side.
enable_testing()

# introduce a target that triggers the building of all tests
add_custom_target(build_tests)

# register new label(s)
function(frackit_register_test_labels)
  include(CMakeParseArguments)
  cmake_parse_arguments(ARG "" "" "LABELS" ${ARGN})

  if( (DEFINED ARG_UNPARSED_ARGUMENTS) AND NOT ( ARG_UNPARSED_ARGUMENTS STREQUAL "" ) )
    message(WARNING "Unparsed arguments in frackit_register_test_labels(): "
                    "<${ARG_UNPARSED_ARGUMENTS}>")
  endif()

  foreach(LABEL IN LISTS ARG_LABELS)
    if (LABEL STREQUAL "")
        message(FATAL_ERROR "Cannot register empty labels")
    endif()
    set(TARGET "build_${LABEL}_tests")
    if(NOT TARGET "${TARGET}")
      add_custom_target("${TARGET}")
    endif()
  endforeach()
endfunction(frackit_register_test_labels)

# function to register a test
function(frackit_register_test)
  include(CMakeParseArguments)
  set(SINGLEARGS NAME TARGET TIMEOUT)
  set(MULTIARGS SOURCES COMPILE_DEFINITIONS CMD_ARGS COMMAND LABELS)
  cmake_parse_arguments(ADDTEST "" "${SINGLEARGS}" "${MULTIARGS}" ${ARGN})

  if(ADDTEST_UNPARSED_ARGUMENTS)
    message(WARNING "Unparsed arguments ('${ADDTEST_UNPARSED_ARGUMENTS}') for frackit_register_test!")
  endif()

  # catch erroneous argument combos
  if(NOT ADDTEST_SOURCES AND NOT ADDTEST_TARGET)
    message(FATAL_ERROR "Neither SOURCES nor TARGET was set for frackit_register_test!")
  endif()
  if(ADDTEST_SOURCES AND ADDTEST_TARGET)
    message(FATAL_ERROR "You cannot set both SOURCES and TARGET for frackit_register_test")
  endif()
  if(NOT ADDTEST_NAME)
    message(FATAL_ERROR "You have to define a name for the test!")
  endif()

  if(NOT ADDTEST_COMMAND)
    set(ADDTEST_COMMAND ${ADDTEST_NAME})
  endif()
  if(NOT ADDTEST_TIMEOUT)
    set(ADDTEST_TIMEOUT 300)
  endif()

  # Add the executable if it is not already present
  if(ADDTEST_SOURCES)
    add_executable(${ADDTEST_NAME} ${ADDTEST_SOURCES})
    target_compile_definitions(${ADDTEST_NAME} PUBLIC ${ADDTEST_COMPILE_DEFINITIONS})
    target_compile_options(${ADDTEST_NAME} PUBLIC ${ADDTEST_COMPILE_FLAGS})

    # link OCC library to each test
    target_link_libraries(${ADDTEST_NAME} ${ADDTEST_LINK_LIBRARIES} ${OCC_LIBS})
    set(ADDTEST_TARGET ${ADDTEST_NAME})
  endif()

  # Add the actual test
  add_test(NAME ${ADDTEST_NAME} COMMAND "${ADDTEST_COMMAND}" ${ADDTEST_CMD_ARGS})

  # Exclude the target from all
  set_property(TARGET ${ADDTEST_TARGET} PROPERTY EXCLUDE_FROM_ALL 1)

  # make sure each label exists and its name is acceptable
  frackit_register_test_labels(LABELS ${ADDTEST_LABELS})

  # Set the labels on this new test
  set_tests_properties(${ADDTEST_NAME} PROPERTIES LABELS "${ADDTEST_LABELS}")

  # register test within the custom targets
  add_dependencies(build_tests ${ADDTEST_TARGET})
  foreach(label IN LISTS ADDTEST_LABELS)
    add_dependencies(build_${label}_tests ${ADDTEST_TARGET})
  endforeach()

endfunction(frackit_register_test)


#######################
# deprecated functions
function(frackit_declare_test_label)
  if(NOT DEFINED FRACKIT_TEST_LABEL_DEPR_WARNING)
    message(WARNING "This function is deprecated. Use frackit_register_test_labels instead!")
    set(FRACKIT_TEST_LABEL_DEPR_WARNING)
  endif()
  frackit_register_test_labels(${ARGV})
endfunction(frackit_declare_test_label)

function(frackit_add_test)
  if(NOT DEFINED FRACKIT_TEST_DEPR_WARNING)
    message(WARNING "This function is deprecated. Use frackit_register_test instead!")
    set(FRACKIT_TEST_DEPR_WARNING)
  endif()
  frackit_register_test(${ARGV})
endfunction(frackit_add_test)
