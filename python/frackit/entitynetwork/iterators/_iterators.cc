// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#include <pybind11/pybind11.h>

#include <frackit/common/id.hh>
#include <frackit/occ/breputilities.hh>
#include <frackit/entitynetwork/entitynetwork.hh>
#include <frackit/python/entitynetwork/traits.hh>

PYBIND11_MODULE(_iterators, module)
{
    namespace py = pybind11;
    using namespace Frackit;
    using T = Python::PythonEntityNetworkTraits;
    using EN = EntityNetwork<T>;
    using Entity = T::EntityFragment;
    using Intersection = T::IntersectionFragment;
    using Junction = T::IntersectionJunction;

    module.def("subDomainFragments",
                 [] (const EN& network)
                 { return py::make_iterator<py::return_value_policy::automatic>(subDomainFragments(network).begin(), subDomainFragments(network).end()); },
                 "Iterator over the subdomain fragments of the given network");
    module.def("entityFragments",
                 [] (const EN& network)
                 { return py::make_iterator<py::return_value_policy::automatic>(entityFragments(network).begin(), entityFragments(network).end()); },
                 "Iterator over the entity fragments of the given network");
    module.def("intersectionFragments",
                 [] (const EN& network)
                 { return py::make_iterator<py::return_value_policy::automatic>(intersectionFragments(network).begin(), intersectionFragments(network).end()); },
                 "Iterator over the intersection fragments of the given network");
    module.def("intersectionJunctions",
                 [] (const EN& network)
                 { return py::make_iterator<py::return_value_policy::automatic>(intersectionJunctions(network).begin(), intersectionJunctions(network).end()); },
                 "Iterator over the intersection junctions of the given network");

    module.def("subDomainFragments",
                 [] (const EN& network, const Id& id)
                 { return py::make_iterator<py::return_value_policy::automatic>(subDomainFragments(network, id).begin(), subDomainFragments(network, id).end()); },
                 "Iterator over the subdomain fragments of the given network that belong to the primary subdomain with the given id");
    module.def("entityFragments",
                 [] (const EN& network, const Id& id)
                 { return py::make_iterator<py::return_value_policy::automatic>(entityFragments(network, id).begin(), entityFragments(network, id).end()); },
                 "Iterator over the entity fragments of the given network that belong to the primary entity with the given id");
    module.def("intersectionFragments",
                 [] (const EN& network, const Id& id)
                 { return py::make_iterator<py::return_value_policy::automatic>(intersectionFragments(network, id).begin(), intersectionFragments(network, id).end()); },
                 "Iterator over the intersection fragments of the given network that belong to the primary intersection with the given id");

    module.def("embeddedFragments",
                 [] (const EN& network, const Entity& ent)
                 { return py::make_iterator<py::return_value_policy::automatic>(embeddedFragments(network, ent).begin(), embeddedFragments(network, ent).end()); },
                 "Iterator over the intersection fragments in which the given entity fragment intersects with other ones");
    module.def("embeddedFragments",
                 [] (const EN& network, const Intersection& is)
                 { return py::make_iterator<py::return_value_policy::automatic>(embeddedFragments(network, is).begin(), embeddedFragments(network, is).end()); },
                 "Iterator over the intersection junctions that the given intersection is connected to");
    module.def("adjacentFragments",
                 [] (const EN& network, const Intersection& is)
                 { return py::make_iterator<py::return_value_policy::automatic>(adjacentFragments(network, is).begin(), adjacentFragments(network, is).end()); },
                 "Iterator over the entity fragments that intersect in the given intersection fragment");
    module.def("adjacentFragments",
                 [] (const EN& network, const Junction& j)
                 { return py::make_iterator<py::return_value_policy::automatic>(adjacentFragments(network, j).begin(), adjacentFragments(network, j).end()); },
                 "Iterator over the entity fragments that intersect in the given intersection fragment");
}
