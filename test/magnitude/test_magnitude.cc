#include <iostream>
#include <vector>
#include <stdexcept>
#include <string>

#include <BRepTools.hxx>

#include <frackit/magnitude/containedmagnitude.hh>
#include <frackit/precision/defaultepsilon.hh>
#include <frackit/occ/breputilities.hh>

#include <frackit/geometry/box.hh>
#include <frackit/geometry/disk.hh>
#include <frackit/geometry/segment.hh>

//! test computation of lengths/areas/volumes
int main()
{
    using namespace Frackit;
    using ctype = double;
    using Disk = Disk<ctype>;
    using Box = Box<ctype>;
    using Point = typename Disk::Point;
    using Direction = typename Disk::Direction;
    using Vector = typename Direction::Vector;
    using Segment = Segment<ctype, 3>;

    // basis directions
    const Direction e1(Vector(1.0, 0.0, 0.0));
    const Direction e2(Vector(0.0, 1.0, 0.0));
    const Direction e3(Vector(0.0, 0.0, 1.0));

    std::vector<ctype> scales({1});
    for (auto f : scales)
    {
        Box box(0.0, 0.0, 0.0, f, f, f);

        // disk outside the box. Area should be zero.
        Disk disk(Point(1.5*f, 0.0, 0.5*f), e1, e2, 0.5*f, 0.5*f);
        if (computeContainedMagnitude(disk, box))
            throw std::runtime_error("Test 1 failed");
        std::cout << "Test 1 passed" << std::endl;

        // disk area should be half of the original one.
        using std::abs;
        Disk disk2(Point(1.0*f, 0.5*f, 0.5*f), e1, e2, 0.5*f, 0.5*f);
        const auto eps = defaultEpsilon(disk2);
        if ( abs(computeContainedMagnitude(disk2, box) - 0.5*disk.area()) > eps*eps)
            throw std::runtime_error("Test 2 failed");
        std::cout << "Test 2 passed" << std::endl;

        const auto eps2 = defaultEpsilon(disk)*defaultEpsilon(disk);
        const auto diskArea = computeMagnitude(disk);
        const auto diskShapeArea = computeMagnitude(OCCUtilities::getShape(disk));
        if (abs(diskShapeArea - diskArea) > eps2)
            throw std::runtime_error("Disk areas not the same");
        std::cout << "Test 3 passed" << std::endl;

        // test one-dimensional geometry
        const auto s1 = Segment(Point(0.0, 0.0, 0.0), Point(1.0*f, 1.0*f, 1.0*f));
        const auto segLength = computeMagnitude(s1);
        const auto segShapeLength = computeMagnitude(OCCUtilities::getShape(s1));
        if (abs(segLength - segShapeLength) > eps)
            throw std::runtime_error("Segment lengths not equal");
        std::cout << "Test 4 passed" << std::endl;

        // test three-dimensional geometry
        const auto boxVol = computeMagnitude(box);
        const auto boxShapeVol = computeMagnitude(OCCUtilities::getShape(box));
        if (abs(boxVol - boxShapeVol) > eps*eps) // TODO: eps^3 doesn't work. How to choose volumetric eps?
            throw std::runtime_error("Box volumes not equal");
        std::cout << "Test 5 passed" << std::endl;

        // check wires and shells
        const auto ellLength = computeMagnitude(disk.boundingEllipse());
        const auto ellShapeLength = computeMagnitude(BRepTools::OuterWire(OCCUtilities::getShape(disk)));
        if (abs(ellLength - ellShapeLength) > eps)
            throw std::runtime_error("Ellipse length not equal");
        std::cout << "Test 6 passed" << std::endl;

        const auto boxSurfArea = box.face(0).area()+box.face(1).area()+box.face(2).area()
                                 +box.face(3).area()+box.face(4).area()+box.face(5).area();
        const auto boxShellArea = computeMagnitude(OCCUtilities::getShells(OCCUtilities::getShape(box))[0]);
        if (abs(boxSurfArea - boxShellArea) > eps*eps)
            throw std::runtime_error("Box surface area not equal");
        std::cout << "Test 7 passed" << std::endl;
    }

    std::cout << "All tests passed" << std::endl;

    return 0;
}
