#include <stdexcept>
#include <iostream>
#include <vector>
#include <string>
#include <cmath>

#include <frackit/geometry/vector.hh>
#include <frackit/geometry/point.hh>
#include <frackit/geometry/direction.hh>
#include <frackit/precision/precision.hh>

template<class G>
G makeGeometry(typename G::ctype x, typename G::ctype y, typename G::ctype z)
{
    // avoid unused warnings
    std::cout << "constructing from coordinates " << x << ", " << y << ", " << z << std::endl;

    static constexpr int spaceDim = G::worldDimension();
    if constexpr(spaceDim == 1) return {x};
    else if constexpr(spaceDim == 2) return {x, y};
    else if constexpr(spaceDim == 3) return {x, y, z};
}

template<int spaceDim, class ctype>
Frackit::Point<ctype, spaceDim> makePoint(ctype x, ctype y, ctype z)
{ return makeGeometry< Frackit::Point<ctype, spaceDim> >(x, y, z); }

template<int spaceDim, class ctype>
Frackit::Vector<ctype, spaceDim> makeVector(ctype x, ctype y, ctype z)
{ return makeGeometry< Frackit::Vector<ctype, spaceDim> >(x, y, z); }

template<class ctype, int spaceDim>
void testVector()
{
    using Vector = Frackit::Vector<ctype, spaceDim>;
    using Direction = Frackit::Direction<ctype, spaceDim>;
    for (const auto f : std::vector<ctype>({1e-10, 1e-5, 1, 1e5, 1e10}))
    {
        const ctype angularEps = Frackit::Precision<ctype>::angular()*f;
        const ctype eps = Frackit::Precision<ctype>::confusion()*f;

        const auto p1 = makePoint<spaceDim>(0.*f, 0.*f, 0.*f);
        const auto p2 = makePoint<spaceDim>(1.*f, 1.*f, 1.*f);
        const auto v1 = makeVector<spaceDim>(1.*f, 1.*f, 1.*f);
        const auto v1_2 = Vector(p1, p2);
        const auto v1_3 = Vector(Direction(v1));

        const auto v0 = Vector(p1, p1);
        const auto v0_2 = Vector();
        if ( abs(v0.length() - v0_2.length()) > eps )
            throw std::runtime_error("Null vectors not equal");

        std::cout << "checking vector with coords: " << v1 << std::endl;
        using std::abs;
        if (abs(v1_3.length() - 1.0) > eps) throw std::runtime_error("Length not 1.0");
        if (!v1.isParallel(v1_2, angularEps)) throw std::runtime_error("Vector not parallel (given eps)");
        if (!v1.isParallel(v1_2)) throw std::runtime_error("Vector not parallel (default eps)");

        auto vCopy = v1; vCopy *= 2.0;
        if ( abs(vCopy.length() - v1.length()*2.0) > eps ) throw std::runtime_error("Length not twice");
        if ( abs(vCopy.length() - v1_2.length()*2.0) > eps ) throw std::runtime_error("Length not twice");

        vCopy /= 4.0;
        if ( abs(vCopy.length() - v1.length()*0.5) > eps ) throw std::runtime_error("Length not half");
        if ( abs(vCopy.length() - v1_2.length()*0.5) > eps ) throw std::runtime_error("Length not half");

        vCopy = v1 + vCopy;
        if ( abs(vCopy.length() - v1.length()*1.5) > eps ) throw std::runtime_error("Length not 1.5 times as long");
        if ( abs(vCopy.length() - v1_2.length()*1.5) > eps ) throw std::runtime_error("Length not 1.5 times as long");

        if constexpr (spaceDim == 3)
        {
            const auto vN = makeOrthogonalVector(v1);
            if ( abs(vN*v1) > eps ) throw std::runtime_error("Scalar product not zero");
        }
    }
}

//! test some functionality of vectors
int main()
{
    using ctype = double;
    testVector<ctype, 1>();
    testVector<ctype, 2>();
    testVector<ctype, 3>();

    std::cout << "All tests passed" << std::endl;
    return 0;
}
