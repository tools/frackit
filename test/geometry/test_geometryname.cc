#include <iostream>
#include <vector>
#include <string>

#include <TopoDS_Shape.hxx>
#include <TopoDS_Vertex.hxx>
#include <TopoDS_Edge.hxx>
#include <TopoDS_Wire.hxx>
#include <TopoDS_Face.hxx>
#include <TopoDS_Shell.hxx>
#include <TopoDS_Solid.hxx>
#include <TopoDS_Compound.hxx>

#include <frackit/geometry/box.hh>
#include <frackit/geometry/boundingbox.hh>
#include <frackit/geometry/circle.hh>
#include <frackit/geometry/cylinder.hh>
#include <frackit/geometry/cylindersurface.hh>
#include <frackit/geometry/direction.hh>
#include <frackit/geometry/disk.hh>
#include <frackit/geometry/ellipsearc.hh>
#include <frackit/geometry/ellipse.hh>
#include <frackit/geometry/hollowcylinder.hh>
#include <frackit/geometry/line.hh>
#include <frackit/geometry/plane.hh>
#include <frackit/geometry/point.hh>
#include <frackit/geometry/polygon.hh>
#include <frackit/geometry/quadrilateral.hh>
#include <frackit/geometry/segment.hh>
#include <frackit/geometry/sphere.hh>
#include <frackit/geometry/triangle.hh>
#include <frackit/geometry/vector.hh>
#include <frackit/geometryutilities/name.hh>

template<class G>
G makeFromCoords(typename G::ctype x, typename G::ctype y, typename G::ctype z)
{
    // avoid unused warnings
    std::cout << "constructing from coordinates " << x << ", " << y << ", " << z << std::endl;

    static constexpr int spaceDim = G::worldDimension();
    if constexpr(spaceDim == 1) return {x};
    else if constexpr(spaceDim == 2) return {x, y};
    else if constexpr(spaceDim == 3) return {x, y, z};
}

template<int spaceDim>
void test()
{
    std::cout << "\nTesting space dimension " << spaceDim << std::endl;
    using namespace Frackit;
    using ctype = double;

    const auto p = makeFromCoords<Point<ctype, spaceDim>>(1.0, 1.0, 1.0);
    const auto v = makeFromCoords<Vector<ctype, spaceDim>>(1.0, 1.0, 1.0);
    const auto d = Direction<ctype, spaceDim>(v);
    const auto l = Line<ctype, spaceDim>(p, d);
    const auto s = Segment<ctype, spaceDim>(p, p+v);

    int i = 1;
    std::cout << i++ << ": " << geometryName(p) << std::endl;
    std::cout << i++ << ": " << geometryName(v) << std::endl;
    std::cout << i++ << ": " << geometryName(d) << std::endl;
    std::cout << i++ << ": " << geometryName(l) << std::endl;
    std::cout << i++ << ": " << geometryName(s) << std::endl;

    // TODO: some of these could/should be implemented in more space dimensions
    if constexpr(spaceDim == 3)
    {
        const auto e1 = Direction<ctype, spaceDim>(makeFromCoords<Vector<ctype, spaceDim>>(1.0, 0.0, 0.0));
        const auto e2 = Direction<ctype, spaceDim>(makeFromCoords<Vector<ctype, spaceDim>>(0.0, 1.0, 0.0));
        const auto c = Circle<ctype, spaceDim>(p, d, 1.0);
        const auto e = Ellipse<ctype, spaceDim>(p, e1, e2, 1.0, 1.0);
        const auto ea = EllipseArc<ctype, spaceDim>(e, e.getPoint(0.0), e.getPoint(0.25));
        const auto q = Quadrilateral<ctype, spaceDim>(e.getPoint(0.0), e.getPoint(0.25), e.getPoint(0.75), e.getPoint(0.5));
        const auto pg = Polygon<ctype, spaceDim>(std::vector<Point<ctype, spaceDim>>({q.corner(0), q.corner(1),
                                                                                      q.corner(3), q.corner(2)}));

        std::cout << i++ << ": " << geometryName(c) << std::endl;
        std::cout << i++ << ": " << geometryName(e) << std::endl;
        std::cout << i++ << ": " << geometryName(ea) << std::endl;

        std::cout << i++ << ": " << geometryName(q) << std::endl;
        std::cout << i++ << ": " << geometryName(pg) << std::endl;
        std::cout << i++ << ": " << geometryName(Triangle<ctype, spaceDim>(pg.corner(0), pg.corner(1), pg.corner(2))) << std::endl;
        std::cout << i++ << ": " << geometryName(Disk<ctype>(e)) << std::endl;
        std::cout << i++ << ": " << geometryName(Plane<ctype, spaceDim>(p, d)) << std::endl;
        std::cout << i++ << ": " << geometryName(Cylinder<ctype>(1.0, 1.0).lateralFace()) << std::endl;


        std::cout << i++ << ": " << geometryName(Box<ctype>(p, p + v)) << std::endl;
        std::cout << i++ << ": " << geometryName(BoundingBox<ctype>(p, p + v)) << std::endl;
        std::cout << i++ << ": " << geometryName(Sphere<ctype>(p, 1.0)) << std::endl;
        std::cout << i++ << ": " << geometryName(Cylinder<ctype>(1.0, 1.0)) << std::endl;
        std::cout << i++ << ": " << geometryName(HollowCylinder(0.5, 1.0, 1.0)) << std::endl;
    }

    std::cout << "Dimension-specific tests passed" << std::endl;
}

int main()
{
    test<1>();
    test<2>();
    test<3>();

    using namespace Frackit;

    int i = 0;
    std::cout << "Testing shape classes" << std::endl;
    std::cout << i++ << ": " << geometryName(TopoDS_Shape()) << std::endl;
    std::cout << i++ << ": " << geometryName(TopoDS_Vertex()) << std::endl;
    std::cout << i++ << ": " << geometryName(TopoDS_Edge()) << std::endl;
    std::cout << i++ << ": " << geometryName(TopoDS_Wire()) << std::endl;
    std::cout << i++ << ": " << geometryName(TopoDS_Face()) << std::endl;
    std::cout << i++ << ": " << geometryName(TopoDS_Shell()) << std::endl;
    std::cout << i++ << ": " << geometryName(TopoDS_Solid()) << std::endl;
    std::cout << i++ << ": " << geometryName(TopoDS_Compound()) << std::endl;

    std::cout << "All tests passed" << std::endl;
}
