#include <stdexcept>
#include <string>
#include <vector>
#include <cmath>

#include <frackit/geometry/point.hh>
#include <frackit/geometry/box.hh>
#include <frackit/geometry/boundingbox.hh>

//! test some functionality of bounding boxes
int main()
{
    using ctype = double;
    using Box = Frackit::Box<ctype>;
    using BoundingBox = Frackit::BoundingBox<ctype>;

    Box box(0., 0., 0., 1., 1., 1.);
    const auto& a = box.corner(0);
    const auto& b = box.corner(7);

    // lambda to test corner equality
    auto testCorners = [&box] (const BoundingBox& bbox,
                               const auto& p0,
                               const auto& p7) -> bool
    {
        if (!bbox.corner(0).isEqual(p0)) return false;
        if (!bbox.corner(7).isEqual(p7)) return false;
        return true;
    };

    // construction from box and corners
    if (!testCorners(BoundingBox(box), a, b)) throw std::runtime_error("Corners not equal");
    if (!testCorners(BoundingBox(a, b), a, b)) throw std::runtime_error("Corners not equal");

    // construction from coordinates
    BoundingBox bbox(a.x(), a.y(), a.z(), b.x(), b.y(), b.z());
    if (!testCorners(bbox, a, b)) throw std::runtime_error("Corners not equal");

    // test bbox merging
    BoundingBox bbox2(-1., -1., -1., 2., 2., 2.);
    BoundingBox bbox3(0.5, 0.5, 0.5, 2., 2., 2.);

    const auto merge1 = bbox + bbox2;
    const auto merge2 = bbox + bbox3;

    if (!testCorners(merge1, bbox2.corner(0), bbox2.corner(7))) throw std::runtime_error("merge1 failed");
    if (!testCorners(merge2, bbox.corner(0), bbox2.corner(7))) throw std::runtime_error("merge2 failed");

    bbox += bbox3;
    if (!testCorners(bbox, merge2.corner(0), merge2.corner(7))) throw std::runtime_error("in-place merge1 failed");

    bbox += bbox2;
    if (!testCorners(bbox, merge1.corner(0), merge1.corner(7))) throw std::runtime_error("in-place merge2 failed");

    std::cout << "All tests passed" << std::endl;

    return 0;
}
