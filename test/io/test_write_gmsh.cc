#include <iostream>
#include <fstream>

#include <frackit/common/id.hh>
#include <frackit/geometry/cylinder.hh>
#include <frackit/geometry/disk.hh>

#include <frackit/entitynetwork/networkbuilder.hh>
#include <frackit/io/gmshwriter.hh>

void testGeoFile(const std::string& fileName,
                 const std::string& testString,
                 std::size_t numOccurrences,
                 bool atBeginning = false)
{
    std::ifstream geoFile(fileName + ".geo");
    if (geoFile.fail()) throw std::runtime_error("Could not open geo file");

    std::size_t foundCount = 0;
    while (!geoFile.eof())
    {
        std::string line;
        std::getline(geoFile, line);
        const auto pos = line.find(testString);
        if (pos != std::string::npos)
            if (!atBeginning || pos == 0)
                foundCount++;
    }

    if (foundCount != numOccurrences)
    {
        std::cout << "Found " << testString << " " << foundCount
                  << " times instead of " << numOccurrences << std::endl;
        throw std::runtime_error("Could not find given number of occurrences");
    }

    std::cout << "Successfully found " << testString << " " << foundCount << " times." << std::endl;
}


template<class Call>
void testException(const Call& call)
{
    bool passed = false;
    try { call(); }
    catch (...) { passed = true; }

    if (!passed) throw std::runtime_error("No exception caught!");
}

//! create 3 disk-shaped fractures and write geo (gmsh file format) file
int main(int argc, char** argv)
{
    using namespace Frackit;
    using ctype = double;
    using Domain = Cylinder<ctype>;
    using Disk = Disk<ctype>;
    using Point = typename Disk::Point;
    using Direction = typename Disk::Direction;
    using Vector = typename Direction::Vector;

    const Direction e1(Vector(1.0, 0.0, 0.0));
    const Direction e2(Vector(0.0, 1.0, 0.0));
    const Direction e3(Vector(0.0, 0.0, 1.0));

    // Domain in which the disks are embedded
    Domain domain(0.5, 1.0);
    Domain domain2(Disk(Point(0.0, 0.0, 1.0), e1, e2, 1.0, 1.0), 1.0);

    // disks to be added
    std::vector<Disk> disks; disks.reserve(7);
    disks.emplace_back(Disk(Point(0.0, 0.0, 0.1), e1, e2, 1.0, 1.0));
    disks.emplace_back(Disk(Point(0.0, 0.1, 0.5), e1, e3, 2.0, 2.0));
    disks.emplace_back(Disk(Point(0.0, 0.0, 0.75), e1, e2, 1.0, 1.0));
    disks.emplace_back(Disk(Point(0.0, 0.0, 0.5), e2, e3, 1.0, 1.0));
    disks.emplace_back(Disk(Point(0.0, 0.0, 1.1), e1, e2, 1.0, 1.0));
    disks.emplace_back(Disk(Point(0.0, -0.1, 1.5), e1, e3, 2.0, 2.0));
    disks.emplace_back(Disk(Point(0.0, 0.0, 1.75), e1, e2, 1.0, 1.0));

    // make and write non-contained network
    EntityNetworkBuilder<ctype> builder; builder.addEntities(disks);
    GmshWriter writer(builder.build());

    // check tag getters
    const auto domainTag = GmshWriter::GeometryTag::subDomain;
    const auto entityTag = GmshWriter::GeometryTag::entity;
    const auto isTag = GmshWriter::GeometryTag::intersection;
    const auto jTag = GmshWriter::GeometryTag::junction;
    if (domainTag != writer.getGeometryTag(3)) throw std::runtime_error("tag mismatch");
    if (entityTag != writer.getGeometryTag(2)) throw std::runtime_error("tag mismatch");
    if (isTag != writer.getGeometryTag(1)) throw std::runtime_error("tag mismatch");
    if (jTag != writer.getGeometryTag(0)) throw std::runtime_error("tag mismatch");

    // test mesh size setters
    writer.setMeshSize(entityTag, 1.0);
    writer.setMeshSize(entityTag, Id(1), 1.0);
    writer.setMeshSize(entityTag, {Id(1), Id(2)}, 1.0);
    writer.setMeshSize(entityTag, std::vector<Id>({Id(1), Id(2)}), 1.0);
    writer.setMeshSize(jTag, Id(1), 1.0);
    writer.setMeshSize(jTag, std::vector<Id>({Id(1), Id(2), Id(3),
                                              Id(4), Id(5), Id(6)}), 1.0);
    testException([&] () { writer.setMeshSize(entityTag, Id(8), 1.0); });
    testException([&] () { writer.setMeshSize(jTag, Id(7), 1.0); });
    writer.write("noncontained");

    // make and write contained network
    ContainedEntityNetworkBuilder<ctype> containedBuilder;
    containedBuilder.addConfiningSubDomain(domain, Id(1));
    containedBuilder.addSubDomainEntity(disks[0],  Id(1));
    containedBuilder.addSubDomainEntity(disks[1],  Id(1));
    containedBuilder.addSubDomainEntity(disks[2], Id(1));
    containedBuilder.addSubDomainEntity(disks[3], Id(1));

    containedBuilder.addConfiningSubDomain(domain2, Id(2));
    containedBuilder.addSubDomainEntity(disks[4],  Id(2));
    containedBuilder.addSubDomainEntity(disks[5],  Id(2));
    containedBuilder.addSubDomainEntity(disks[6], Id(2));

    // test construction with gmsh version
    writer = GmshWriter(containedBuilder.build(), 4, 0);

    // test some actions that should cause exceptions to be raised
    testException([&] () { writer.setMeshSize(domainTag, Id(3), 0.1); });
    testException([&] () { writer.setMeshSize(domainTag, {Id(2), Id(3)}, 0.1); });
    testException([&] () { writer.setMeshSize(entityTag, Id(8), 0.1); });
    testException([&] () { writer.setMeshSize(entityTag, {Id(5), Id(8)}, 0.1); });
    testException([&] () { writer.setMeshSize(isTag, Id(199), 0.1); });
    testException([&] () { writer.setMeshSize(jTag, Id(7), 0.1); });

    // test mesh size setters
    writer.setMeshSize(domainTag, 0.1);
    writer.setMeshSize(domainTag, {Id(1), Id(2)}, 0.5);
    writer.setMeshSize(domainTag, std::vector<Id>({Id(1), Id(2)}), 0.5);
    writer.setMeshSize(domainTag, Id(1), 0.1);

    writer.setMeshSize(entityTag, 0.5);
    writer.setMeshSize(entityTag, {Id(5), Id(6)}, 0.5);
    writer.setMeshSize(entityTag, std::vector<Id>({Id(5), Id(6)}), 0.5);
    writer.setMeshSize(entityTag, Id(1), 0.05);
    writer.setMeshSize(entityTag, Id(2), 0.1);
    writer.setMeshSize(entityTag, Id(3), 0.2);
    writer.setMeshSize(entityTag, Id(4), 0.3);
    writer.setMeshSize(entityTag, Id(5), 0.4);
    writer.setMeshSize(entityTag, Id(6), 0.5);
    writer.setMeshSize(entityTag, Id(7), 0.6);
    writer.setMeshSize(jTag, Id(1), 0.01);
    writer.setMeshSize(jTag, Id(2), 0.2);

    const int numVolumes = 2;
    const int numSurfaces = 7;
    const int numCurves = 13;
    const int numPoints = 2;
    const int allPhysicals = numVolumes+numSurfaces+numCurves+numPoints;

    // check current mesh file status
    writer.write("contained");
    testGeoFile("contained", "Physical Volume", numVolumes);
    testGeoFile("contained", "Physical Surface", numSurfaces);
    testGeoFile("contained", "Physical Curve", numCurves);
    testGeoFile("contained", "Physical Point", numPoints);
    testGeoFile("contained", "Characteristic Length", numVolumes+numSurfaces+numPoints);

    // define mesh size at curves
    for (int i = 0; i < numCurves; ++i)
        writer.setMeshSize(isTag, Id(i+1), 0.01*(i+1));
    writer.write("contained");
    testGeoFile("contained", "Characteristic Length", allPhysicals);
    testGeoFile("contained", "DefineConstant[ intersectionMeshSize_", numCurves, true);
    writer.setMeshSize(isTag, 0.01);
    writer.write("contained");
    testGeoFile("contained", "Characteristic Length", allPhysicals);
    testGeoFile("contained", "DefineConstant[ intersectionMeshSize_", 1, true);

    // successively remove physical definitions
    writer.setNonPhysical(isTag); writer.write("contained");
    testGeoFile("contained", "Physical Curve", 0);
    writer.setNonPhysical(entityTag); writer.write("contained");
    testGeoFile("contained", "Physical Surface", 0);
    writer.setNonPhysical(domainTag); writer.write("contained");
    testGeoFile("contained", "Physical Volume", 0);
    writer.setNonPhysical(jTag); writer.write("contained");
    testGeoFile("contained", "Physical Point", 0);

    // test other file format
    testGeoFile("contained", "Characteristic Length", allPhysicals);
    testGeoFile("contained", "MeshSize{", 0);
    writer.setGmshVersion(4, 7); writer.write("contained");
    testGeoFile("contained", "Characteristic Length", 0);
    testGeoFile("contained", "MeshSize{", allPhysicals);

    // test number of defined mesh size variables
    testGeoFile("contained", "DefineConstant[ subDomainMeshSize_", numVolumes, true);
    testGeoFile("contained", "DefineConstant[ entityMeshSize_", numSurfaces, true);
    writer.setMeshSize(entityTag, Id(2), 0.05); writer.write("contained");
    testGeoFile("contained", "DefineConstant[ entityMeshSize_", numSurfaces-1, true);
    writer.setMeshSize(domainTag, Id(2), 0.1); writer.write("contained");
    testGeoFile("contained", "DefineConstant[ subDomainMeshSize_", numVolumes-1, true);
    writer.resetMeshSizes(isTag); writer.write("contained");
    testGeoFile("contained", "DefineConstant[ intersectionMeshSize_", 0);

    // make things physical again
    writer.setPhysical(domainTag, Id(1)); writer.write("contained");
    testGeoFile("contained", "Physical Volume", 1);
    writer.setPhysical(domainTag); writer.write("contained");
    testGeoFile("contained", "Physical Volume", numVolumes);
    writer.setPhysical(entityTag, Id(1)); writer.write("contained");
    testGeoFile("contained", "Physical Surface", 1);
    writer.setPhysical(entityTag); writer.write("contained");
    testGeoFile("contained", "Physical Surface", numSurfaces);
    writer.setPhysical(isTag); writer.write("contained");
    testGeoFile("contained", "Physical Curve", numCurves);

    std::cout << "All tests passed" << std::endl;

    return 0;
}
