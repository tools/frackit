#include <stdexcept>
#include <string>
#include <vector>
#include <cmath>

#include <BRepTools.hxx>
#include <BRep_Builder.hxx>
#include <TopoDS_Compound.hxx>

#include <frackit/sampling/makeuniformpointsampler.hh>
#include <frackit/geometry/point.hh>
#include <frackit/geometry/box.hh>
#include <frackit/geometry/boundingbox.hh>
#include <frackit/geometry/cylinder.hh>
#include <frackit/geometry/hollowcylinder.hh>
#include <frackit/geometryutilities/getboundingbox.hh>
#include <frackit/occ/breputilities.hh>

//! sample points on a geometry
//! if a brep file name is provided, the result is written
//! into a brep file, such that the result can be checked visually
template<class G>
void testPointSampling(const G& geometry,
                       std::size_t numSamples,
                       const std::string& brepFileName = "")
{
    using namespace Frackit;
    using ctype = typename G::ctype;
    static constexpr int worldDim = G::worldDimension();

    // sample points
    auto pointSampler = makeUniformPointSampler(geometry);
    std::vector<Frackit::Point<ctype, worldDim>> points(numSamples);
    for (unsigned int i = 0; i < numSamples; ++i) points[i] = pointSampler();

    // check if all of the points are contained in the geometry
    if ( std::any_of(points.begin(), points.end(),
                     [&geometry] (const auto& p)
                     { return !geometry.contains(p); }) )
        throw std::runtime_error("Point not contained in geometry");

    // compute bounding box of the point cloud
    BoundingBox<ctype> bbpc;
    for (const auto& p : points)
        bbpc += getBoundingBox(p);

    // we expect less than 5% deviation to the geometry's bbox
    const auto bbg = getBoundingBox(geometry);
    const auto dx = bbg.xMax() - bbg.xMin();
    const auto dy = bbg.yMax() - bbg.yMin();
    const auto dz = bbg.zMax() - bbg.zMin();

    using std::abs;
    if ( abs(bbpc.xMin() - bbg.xMin()) > 0.05*dx ) throw std::runtime_error("Bounding box deviation > 5%");
    if ( abs(bbpc.xMax() - bbg.xMax()) > 0.05*dx ) throw std::runtime_error("Bounding box deviation > 5%");
    if ( abs(bbpc.yMin() - bbg.yMin()) > 0.05*dy ) throw std::runtime_error("Bounding box deviation > 5%");
    if ( abs(bbpc.yMax() - bbg.yMax()) > 0.05*dy ) throw std::runtime_error("Bounding box deviation > 5%");
    if ( abs(bbpc.zMin() - bbg.zMin()) > 0.05*dz ) throw std::runtime_error("Bounding box deviation > 5%");
    if ( abs(bbpc.zMax() - bbg.zMax()) > 0.05*dz ) throw std::runtime_error("Bounding box deviation > 5%");

    std::cout << "Successfully tested geometry: " << geometry.name() << std::endl;
    std::cout << "\t- Deviation in x-direction: " << abs(dx - bbpc.xMax() + bbpc.xMin())/dx*100.0 << "%" << std::endl;
    std::cout << "\t- Deviation in y-direction: " << abs(dy - bbpc.yMax() + bbpc.yMin())/dy*100.0 << "%" << std::endl;
    std::cout << "\t- Deviation in z-direction: " << abs(dz - bbpc.zMax() + bbpc.zMin())/dz*100.0 << "%" << std::endl;

    if (brepFileName != "")
    {
        // create a single compound and write to .brep file
        BRep_Builder b;
        TopoDS_Compound c;
        b.MakeCompound(c);
        for (const auto& p : points)
            b.Add(c, OCCUtilities::getShape(p));
        b.Add(c, OCCUtilities::getShape(geometry));

        BRepTools::Write(c, brepFileName.c_str());
    }
}

//! test random sampling of points on geometries
int main()
{
    using ctype = double;

    using namespace Frackit;
    using Box = Box<ctype>;
    using BBox = BoundingBox<ctype>;
    using Cylinder = Cylinder<ctype>;
    using HollowCyl = HollowCylinder<ctype>;

    testPointSampling(Box(0.,0.,0.,1.,1.,1.), 1000, "pointsamples_box.brep");
    testPointSampling(BBox(0.,0.,0.,1.,1.,1.), 1000, "pointsamples_bbox.brep");
    testPointSampling(Cylinder(0.5, 1.0), 1000, "pointsamples_cyl.brep");
    testPointSampling(HollowCyl(0.4, 0.5, 1.0), 1000, "pointsamples_hollowcyl.brep");

    std::cout << "All tests passed" << std::endl;
    return 0;
}
